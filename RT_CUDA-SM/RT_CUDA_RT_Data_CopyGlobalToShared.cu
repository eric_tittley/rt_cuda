#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_CUDA.h"

__device__ void RT_CUDA_RT_Data_CopyGlobalToShared(RT_Data const* const Data_GM,
                                                   size_t const losoffset,
                                                   RT_Data* const Data_SM) {
  /* This function takes advantage of coherent memory to move the memory a
   * block size at a time. */

  size_t iz;
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->R[iz] = Data_GM->R[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->dR[iz] = Data_GM->dR[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->Density[iz] = Data_GM->Density[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->Entropy[iz] = Data_GM->Entropy[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->T[iz] = Data_GM->T[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->n_H[iz] = Data_GM->n_H[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->f_H1[iz] = Data_GM->f_H1[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->f_H2[iz] = Data_GM->f_H2[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->n_He[iz] = Data_GM->n_He[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->f_He1[iz] = Data_GM->f_He1[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->f_He2[iz] = Data_GM->f_He2[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->f_He3[iz] = Data_GM->f_He3[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->column_H1[iz] = Data_GM->column_H1[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->column_He1[iz] = Data_GM->column_He1[losoffset + iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_SM->column_He2[iz] = Data_GM->column_He2[losoffset + iz];
  }
#if (defined REFINEMENTS) || (defined SEGMENTED_LOS)
  for (iz = threadIdx.x; iz < 12; iz += blockDim.x) {
    Data_SM->NCol[iz] = Data_GM->NCol[losoffset + iz];
  }
#endif
}
