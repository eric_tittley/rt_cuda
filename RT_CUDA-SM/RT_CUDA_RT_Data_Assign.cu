/* Assign pointers in a RT_Data structure to offsets within its associated
 * memory.
 *
 * ARGUMENTS
 *  Data        Pointer to the RT_Data structure.
 *
 * SEE ALSO
 *  RT_Data_Free, RT_Data_Assign
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_CUDA.h"

__device__ void RT_CUDA_RT_Data_Assign(RT_Data *const Data) {
  rt_float *P = (rt_float *)Data->MemSpace;

  Data->R = P;
  Data->dR = (P += Data->NumCells);
  Data->Density = (P += Data->NumCells);
  Data->Entropy = (P += Data->NumCells);
  Data->T = (P += Data->NumCells);
  Data->n_H = (P += Data->NumCells);
  Data->f_H1 = (P += Data->NumCells);
  Data->f_H2 = (P += Data->NumCells);
  Data->n_He = (P += Data->NumCells);
  Data->f_He1 = (P += Data->NumCells);
  Data->f_He2 = (P += Data->NumCells);
  Data->f_He3 = (P += Data->NumCells);
  Data->column_H1 = (P += Data->NumCells);
  Data->column_He1 = (P += Data->NumCells);
  Data->column_He2 = (P += Data->NumCells);
#ifdef RT_DATA_VELOCITIES
  Data->v_z = (P += Data->NumCells);
  Data->v_x = (P += Data->NumCells);
#endif
#if (defined REFINEMENTS) || (defined SEGMENTED_LOS)
  Data->NCol = (P += Data->NumCells);
#endif
#ifdef REFINEMENTS
  Data->Ref_pointer = (RT_Data **)(P += 12);
#endif
}
