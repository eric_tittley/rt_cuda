#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_CUDA.h"

__device__ void RT_CUDA_RT_Data_CopySharedToGlobal(RT_Data const* const Data_SM,
                                                   size_t const losoffset,
                                                   RT_Data* const Data_GM) {
  size_t iz;
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->R[losoffset + iz] = Data_SM->R[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->dR[losoffset + iz] = Data_SM->dR[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->Density[losoffset + iz] = Data_SM->Density[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->Entropy[losoffset + iz] = Data_SM->Entropy[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->T[losoffset + iz] = Data_SM->T[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->n_H[losoffset + iz] = Data_SM->n_H[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->f_H1[losoffset + iz] = Data_SM->f_H1[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->f_H2[losoffset + iz] = Data_SM->f_H2[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->n_He[losoffset + iz] = Data_SM->n_He[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->f_He1[losoffset + iz] = Data_SM->f_He1[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->f_He2[losoffset + iz] = Data_SM->f_He2[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->f_He3[losoffset + iz] = Data_SM->f_He3[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->column_H1[losoffset + iz] = Data_SM->column_H1[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->column_He1[losoffset + iz] = Data_SM->column_He1[iz];
  }
  for (iz = threadIdx.x; iz < Data_GM->NumCells; iz += blockDim.x) {
    Data_GM->column_He2[losoffset + iz] = Data_SM->column_He2[iz];
  }
#if (defined REFINEMENTS) || (defined SEGMENTED_LOS)
  for (iz = threadIdx.x; iz < 12; iz += blockDim.x) {
    Data_GM->NCol[losoffset + iz] = Data_SM->NCol[iz];
  }
#endif
}
