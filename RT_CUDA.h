#ifndef _RT_CUDA_H_
#define _RT_CUDA_H_

#include <cuda.h>
#include <cuda_runtime.h>

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include "Logic.h"
#include "RT_ConstantsT.h"
#include "RT_Data.h"
#include "RT_IntegralInvolatilesT.h"
#include "RT_Luminosity.h"
#include "RT_Precision.h"
#include "RT_RatesT.h"
#include "RT_SourceT.h"
#include "RT_TimeScale.h"

#ifdef __cplusplus
}
#endif

#include "RT_CUDA_LOS_BlockT.h"

typedef rt_float (*DeviceSourceFunctionT)(RT_LUMINOSITY_ARGS);

typedef struct {
  rt_float I_H1, I_He1, I_He2;  // Photoionization rates [s^-1]
  rt_float G_H1, G_He1, G_He2;  // Heating rates [W]
  rt_float Gamma_HI;            // Photoionization per particle [m^3 s^-1 Hz^-1]
} RT_CUDA_PhotoRates;

/* Function Declarations */
__device__ void RT_CUDA_AdaptiveTimescale(size_t const losid,
                                          size_t const iz,
                                          size_t const cellindex,
                                          rt_float const dt,
                                          RT_ConstantsT const *const Constants,
                                          RT_RatesT const *const Rates,
                                          RT_Data const *const Data_dev,
                                          rt_float *const AdapScales);

__device__ RT_CUDA_PhotoRates
RT_CUDA_Attenuate(RT_ConstantsT const *const Constants,
                  RT_SourceT const *const Source,
                  rt_float const ExpansionFactor,
                  rt_float const DistanceFromSourceMpc,
                  rt_float const DistanceThroughElementMpc,
                  rt_float const ParticleRadiusMpc,
                  RT_CUDA_PhotoRates UnattenuatedRates);

__device__ void RT_CUDA_ConstantsPrint(RT_ConstantsT const *const Constants);

void RT_CUDA_CoolingRates(rt_float const ExpansionFactor,
                          RT_ConstantsT const Constants,
                          RT_Data const Data_dev_RT,
                          /*@out@*/ RT_RatesBlockT const Rates_dev);

__device__ void RT_CUDA_CoolingRatesExtra(
    RT_ConstantsT const *const Constants,
    size_t const cellindex,
    rt_float const ExpansionFactor,
    RT_Data const *const Data_dev,
    /*@out@*/ RT_RatesBlockT *const Rates_dev);

void RT_CUDA_CopyRatesDeviceToHost(RT_RatesBlockT const Rates_dev,
                                   RT_RatesBlockT const Rates_host);

__device__ rt_float
RT_CUDA_CosmologicalTime(rt_float const a,
                         RT_ConstantsT const *const Constants);

__device__ void RT_CUDA_RT_Data_Assign(RT_Data *const Data);

__device__ void RT_CUDA_RT_Data_CopyGlobalToShared(
    RT_Data const *const Data_dev,
    size_t const losoffset,
    RT_Data *const Data_LOS);

__device__ void RT_CUDA_RT_Data_CopySharedToGlobal(
    RT_Data const *const Data_LOS,
    size_t const losoffset,
    RT_Data *const Data_dev);

__device__ rt_float RT_CUDA_H1_H2(rt_float const nu,
                                  RT_ConstantsT const *const Constants);
__device__ rt_float RT_CUDA_He1_He2(rt_float const nu,
                                    RT_ConstantsT const *const Constants);
__device__ rt_float RT_CUDA_He2_He3(rt_float const nu,
                                    RT_ConstantsT const *const Constants);

__device__ void RT_CUDA_CumulativeSumInPlaceOverBlock(rt_float *const A,
                                                      size_t const BlockWidth);

__device__ void print_cell(RT_Cell const *const Data_cell);

void RT_CUDA_initializeEquilibriumValues(RT_Data const Data,
                                         RT_RatesBlockT Rates);

__device__ rt_float RT_CUDA_InterpolateFromTable(rt_float const xp,
                                                 int const N,
                                                 rt_float const *const x,
                                                 rt_float const *const y);

__device__ bool RT_CUDA_isequal(rt_float const n, rt_float const d);

__device__ void RT_CUDA_LastIterationUpdate(
    size_t const cellindex,
    RT_ConstantsT const *const Constants,
    RT_RatesT const *const Rates,
    RT_Data const *const Data_dev);

__device__ rt_float
RT_CUDA_LightFrontPosition(rt_float const t,
                           rt_float const t_on,
                           rt_float const DistanceTolerance,
                           RT_ConstantsT const *const Constants);

#if 0
__device__ rt_float
RT_CUDA_Luminosity_BLACK_BODY( RT_LUMINOSITY_ARGS );
__device__ rt_float
RT_CUDA_Luminosity_HYBRID( RT_LUMINOSITY_ARGS );
__device__ rt_float
RT_CUDA_Luminosity_POWER_LAW_MINUS_ONE_HALF( RT_LUMINOSITY_ARGS );
__device__ rt_float
RT_CUDA_Luminosity_POWER_LAW_MINUS_ONE( RT_LUMINOSITY_ARGS );
__device__ rt_float
RT_CUDA_Luminosity_POWER_LAW_MINUS_THREE_HALVES( RT_LUMINOSITY_ARGS );
__device__ rt_float
RT_CUDA_Luminosity_POWER_LAW_MINUS_TWO( RT_LUMINOSITY_ARGS );
__device__ rt_float
RT_CUDA_Luminosity_POWER_LAW( RT_LUMINOSITY_ARGS );
__device__ rt_float
RT_CUDA_Luminosity_SOURCE_MINI_QUASAR( RT_LUMINOSITY_ARGS );
__device__ rt_float
RT_CUDA_Luminosity_MONOCHROMATIC( RT_LUMINOSITY_ARGS );
__device__ rt_float
RT_CUDA_Luminosity_TABULATED( RT_LUMINOSITY_ARGS );
#endif

__device__ int RT_CUDA_NextTimeStep(rt_float const dt,
                                    rt_float const *const t_step,
                                    rt_float *const dt_RT,
                                    bool *const LastIterationFlag,
                                    rt_float const MIN_DT,
                                    rt_float const *const AdapScales,
                                    size_t const losid,
                                    size_t const numcells,
                                    rt_float const Timestep_Factor);

__device__ RT_CUDA_PhotoRates RT_CUDA_PhotoionizationRates(
    rt_float const N_H1,
    rt_float const N_He1,
    rt_float const N_He2,
    rt_float const N_H1_cum,
    rt_float const N_He1_cum,
    rt_float const N_He2_cum,
    rt_float const ExpansionFactor,
    RT_ConstantsT const *const Constants,
    RT_SourceT const *const Source,
    rt_float const DistanceFromSourceMpc,
    rt_float const ParticleRadiusMpc,
    rt_float const DistanceThroughElementMpc,
    RT_IntegralInvolatilesT const *const Involatiles_dev);

__device__ RT_CUDA_PhotoRates
RT_CUDA_PhotoionizationRatesFunctions(rt_float const nu,
                                      rt_float const L_unattenuated,
                                      rt_float const N_H1,
                                      rt_float const N_He1,
                                      rt_float const N_He2,
                                      rt_float const N_H1_cum,
                                      rt_float const N_He1_cum,
                                      rt_float const N_He2_cum,
                                      RT_ConstantsT const *const Constants,
                                      rt_float const *const Alpha);

__device__ RT_CUDA_PhotoRates RT_CUDA_PhotoionizationRatesIntegrate(
    rt_float const N_H1,
    rt_float const N_He1,
    rt_float const N_He2,
    rt_float const N_H1_cum,
    rt_float const N_He1_cum,
    rt_float const N_He2_cum,
    RT_ConstantsT const *const Constants,
    RT_IntegralInvolatilesT const *const Involatiles_dev);

__device__ RT_CUDA_PhotoRates RT_CUDA_PhotoionizationRatesMonochromatic(
    rt_float const N_H1,
    rt_float const N_He1,
    rt_float const N_He2,
    rt_float const N_H1_cum,
    rt_float const N_He1_cum,
    rt_float const N_He2_cum,
    RT_ConstantsT const *const Constants,
    RT_IntegralInvolatilesT const *const Involatiles,
    /*@out@*/ rt_float *const f);

__device__ void RT_CUDA_ProcessCell(size_t const losid,
                                    size_t const iz,
                                    size_t const cellindex,
                                    bool const LastIterationFlag,
                                    rt_float const ExpansionFactor,
                                    rt_float const tCurrent,
                                    rt_float const dt,
                                    rt_float const dt_RT,
                                    RT_SourceT const *const Source,
                                    RT_ConstantsT const *const Constants,
                                    RT_Data *const Data_dev,
                                    rt_float *const AdapScales);

__device__ void RT_CUDA_ProcessCells(bool const LastIterationFlag,
                                     rt_float const ExpansionFactor,
                                     rt_float const tCurrent,
                                     rt_float const dt,
                                     rt_float const dt_RT,
                                     RT_SourceT const *const Source,
                                     RT_ConstantsT const *const Constants,
                                     RT_Data *const Data,
                                     size_t const losid,
                                     size_t const losoffset,
                                     rt_float *const AdapScales);

int RT_CUDA_ProcessLOSs(rt_float const t,
                        rt_float const dt,
                        rt_float const ExpansionFactor,
                        RT_SourceT const Source,
                        RT_ConstantsT const Constants,
                        RT_Data **const Data_los,
                        RT_CUDA_LOS_BlockT const *const blocks);

__device__ void RT_CUDA_Rates(size_t const cellindex,
                              rt_float const ExpansionFactor,
                              rt_float const tCurrent,
                              RT_SourceT const *const Source,
                              RT_ConstantsT const *const Constants,
                              RT_Data const *const Data_dev,
                              /*@out@*/ RT_RatesT *const Rates);

__device__ void RT_CUDA_Rates_RatesBlock(
    size_t const cellindex,
    rt_float const ExpansionFactor,
    rt_float const tCurrent,
    RT_SourceT const *const Source,
    RT_ConstantsT const *const Constants,
    RT_Data const *const Data_dev,
    /*@out@*/ RT_RatesBlockT *const Rates_dev);

RT_RatesBlockT RT_CUDA_RatesBlock_Allocate(size_t const nParticles);

void RT_CUDA_RatesBlock_Free(RT_RatesBlockT Rates);

__device__ rt_float RT_CUDA_recomb_H1(rt_float const T,
                                      int const recomb_approx);
__device__ rt_float RT_CUDA_recomb_He1(rt_float const T,
                                       int const recomb_approx);
__device__ rt_float RT_CUDA_recomb_He2(rt_float const T,
                                       int const recomb_approx);
__device__ rt_float RT_CUDA_recombcool_H1(rt_float const T,
                                          int const recomb_approx);
__device__ rt_float RT_CUDA_recombcool_He1(rt_float const T,
                                           int const recomb_approx);
__device__ rt_float RT_CUDA_recombcool_He2(rt_float const T,
                                           int const recomb_approx);

__device__ void RT_CUDA_RecombinationCoefficients(
    rt_float const tau_H1_cell,
    rt_float const T,
    /*@out@*/ rt_float *const alpha_H1,
    /*@out@*/ rt_float *const alpha_He1,
    /*@out@*/ rt_float *const alpha_He2,
    /*@out@*/ rt_float *const beta_H1,
    /*@out@*/ rt_float *const beta_He1,
    /*@out@*/ rt_float *const beta_He2);

__device__ void RT_CUDA_RecombinationRatesInclCooling(
    size_t const cellindex,
    rt_float const ExpansionFactor,
    RT_ConstantsT const *const Constants,
    RT_Data const *const Data_dev,
    /*@out@*/ RT_RatesBlockT *const Rates_dev);

void RT_CUDA_RT_Data_CheckData(RT_Data const Data_dev,
                               const char * FileName,
                               int const LineNumber);

__device__ void RT_CUDA_SetSourceFunction(RT_SourceT *const Source);

int RT_CUDA_SetFrequenciesWeightsLuminosities(
    RT_SourceT *const Source,
    RT_ConstantsT const *const Constants,
    rt_float const RedShift,
    rt_float **nu_d,
    rt_float **W_d,
    rt_float **L_d);

__device__ void RT_CUDA_UpdateCell(rt_float const dt_RT,
                                   rt_float const ExpansionFactor,
                                   size_t const cellindex,
                                   RT_ConstantsT const *const Constants,
                                   RT_RatesT const *const Rates,
                                   RT_Data *const Data_dev);

__device__ void RT_CUDA_UpdateCell_RateBlock(
    rt_float const dt_RT,
    rt_float const ExpansionFactor,
    size_t const cellindex,
    RT_ConstantsT const *const Constants,
    RT_RatesBlockT const *const Rates,
    /* updated */ RT_Data *Data);

__device__ void RT_CUDA_UpdateColumnDensitiesAlongLOS(
    size_t const losoffset,
    rt_float const t_step,
    rt_float const dt,
    RT_ConstantsT const *const Constants,
    RT_Data const *const Data_dev);

__device__ rt_float
RT_CUDA_expansion_factor_from_time(rt_float const t,
                                   RT_ConstantsT const *const Constants);

__device__ rt_float
RT_CUDA_EntropyFromTemperature(rt_float const Temperature,
                               size_t const iCell,
                               RT_Data const *const Data,
                               RT_ConstantsT const *const Constants);

__device__ rt_float
RT_CUDA_TemperatureFromEntropy(rt_float const Entropy,
                               size_t const iCell,
                               RT_Data const *const Data,
                               RT_ConstantsT const *const Constants);

void RT_CUDA_setHydrogenEquilibriumValues(RT_ConstantsT const Constants,
                                          RT_Data const Data,
                                          RT_RatesBlockT const Rates);
__device__ bool RT_CUDA_useEquilibriumValues(
    size_t const cellindex,
    RT_Data const *const Data,
    RT_RatesBlockT const *const Rates,
    RT_ConstantsT const *const Constants);

__device__ void RT_CUDA_CollisionalIonizationRate(
    RT_ConstantsT const *const Constants,
    size_t const cell,
    rt_float const alpha_H1,
    rt_float const alpha_He1,
    rt_float const alpha_He2,
    RT_Data const *const Data,
    RT_RatesBlockT *Rates);

#ifdef __cplusplus
extern "C" {
#endif

int RT_CUDA_CopyConstantsToDevice(RT_ConstantsT const Constants,
                                  RT_ConstantsT *Constants_dev);
int RT_CUDA_FreeConstantsOnDevice(RT_ConstantsT const Constants,
                                  RT_ConstantsT *Constants_dev);
int RT_CUDA_allocateIntegralInvolatilesOnDevice(
    RT_IntegralInvolatilesT const Involatiles_h,
    RT_IntegralInvolatilesT *const Involatiles_d);
int RT_CUDA_freeIntegralInvolatilesOnDevice(
    RT_IntegralInvolatilesT Involatiles_d);
int RT_CUDA_copyIntegralInvolatilesToDevice(
    RT_IntegralInvolatilesT const Involatiles_h,
    RT_IntegralInvolatilesT const Involatiles_d);

#ifdef __cplusplus
}
#endif

#ifdef __CUDACC__
#  include "RT_CUDA-common/RT_CUDA_Attenuate.cuh"
#  include "RT_CUDA-common/RT_CUDA_CollisionalIonizationRate.cuh"
#  include "RT_CUDA-common/RT_CUDA_CoolingRatesExtra.cuh"
#  include "RT_CUDA-common/RT_CUDA_EntropyFromTemperature.cuh"
#  include "RT_CUDA-common/RT_CUDA_PhotoionizationRates.cuh"
#  include "RT_CUDA-common/RT_CUDA_PhotoionizationRatesFunctions.cuh"
#  include "RT_CUDA-common/RT_CUDA_PhotoionizationRatesIntegrate.cuh"
#  include "RT_CUDA-common/RT_CUDA_RecombCoef.cuh"
#  include "RT_CUDA-common/RT_CUDA_RecombinationCoefficients.cuh"
#  include "RT_CUDA-common/RT_CUDA_RecombinationRatesInclCooling.cuh"
#  include "RT_CUDA-common/RT_CUDA_TemperatureFromEntropy.cuh"
#  include "RT_CUDA-common/RT_CUDA_UpdateCell_RateBlock.cuh"
#  include "RT_CUDA-common/RT_CUDA_useEquilibriumValues.cuh"
#endif

#endif /* _RT_CUDA_H_ */
