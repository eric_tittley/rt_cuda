ifndef MAKE_CONFIG
 MAKE_CONFIG = ../make.config
endif
include $(MAKE_CONFIG)
# Expected definitions:
#  MAKE_CONFIG, DEFS, ROOT_DIR
#  INCS_BASE, INC_RT, INC_RT_CUDA
#  AR, AR_FLAGS
#  NVCC, NVCC_ARCH

# Required for newer versions of gcc (~4.9+) due to a bug in glibc 2.23.
# See e.g. https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=822783
#DEFS += -D_FORCE_INLINES

INCS = $(INCS_BASE) $(INC_RT) $(INC_RT_CUDA) $(INC_LIBCUDASUPPORT)

LIBRARY = libRT_CUDA.a

LIBRARY_COMMON     = libRT_CUDAcommon.a
LIBRARY_COMMON_DIR = RT_CUDA-common

LIBRARY_SM     = libRT_CUDASM.a
LIBRARY_SM_DIR = RT_CUDA-SM

LIBRARY_noSM     = libRT_CUDAnoSM.a
LIBRARY_noSM_DIR = RT_CUDA-noSM

# GPU Memory model
# Options are
#  SM for shared memory
#  noSM for using only global memory (deprecated?)
RT_CUDA_MEMORY_METHOD = SM

ifeq ($(RT_CUDA_MEMORY_METHOD),SM)
 LIBRARY_MEMORY_MODEL_SPECIFIC = $(LIBRARY_SM_DIR)/$(LIBRARY_SM)
else
 LIBRARY_MEMORY_MODEL_SPECIFIC = $(LIBRARY_noSM_DIR)/$(LIBRARY_noSM)
endif
SUBLIBRARIES = $(LIBRARY_COMMON_DIR)/$(LIBRARY_COMMON) \
               $(LIBRARY_MEMORY_MODEL_SPECIFIC)

SUBDIRS = $(LIBRARY_COMMON_DIR) $(LIBRARY_SM_DIR) $(LIBRARY_noSM_DIR)

all: $(LIBRARY)

$(LIBRARY): $(SUBLIBRARIES) link.o
	-rm -fr tmp
	mkdir tmp
	cd tmp; $(AR) $(AR_FLAGS) x ../$(LIBRARY_COMMON_DIR)/$(LIBRARY_COMMON)
	cd tmp; $(AR) $(AR_FLAGS) x ../$(LIBRARY_MEMORY_MODEL_SPECIFIC)
	$(AR) $(AR_FLAGS) r $(LIBRARY) tmp/*.o link.o
	rm -fr tmp

link.o: $(SUBLIBRARIES)
	$(NVCC) $(NVCC_ARCH) -dlink -o link.o $(SUBLIBRARIES) 

$(LIBRARY_COMMON_DIR)/$(LIBRARY_COMMON):
	$(MAKE) ROOT_DIR=${ROOT_DIR} -C $(LIBRARY_COMMON_DIR)

$(LIBRARY_SM_DIR)/$(LIBRARY_SM):
	$(MAKE) ROOT_DIR=${ROOT_DIR} -C $(LIBRARY_SM_DIR)

$(LIBRARY_noSM_DIR)/$(LIBRARY_noSM):
	$(MAKE) ROOT_DIR=${ROOT_DIR} -C $(LIBRARY_noSM_DIR)

.PHONY: clean
clean:
	-rm *.o
	-rm *~
	-rm link.o
	-rm -fr tmp
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir clean ) \
	done

.PHONY: distclean
distclean: clean
	-rm $(LIBRARY)
	-rm dep.mak
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir distclean ) \
	done

.PHONY: indent
indent:
	indent *.c
	indent *.cpp
	indent *.h

.PHONY: clang-format
clang-format:
	clang-format -i *.h
	$(MAKE) -C RT_CUDA-common clang-format
	$(MAKE) -C RT_CUDA-noSM clang-format
	$(MAKE) -C RT_CUDA-SM clang-format
