#ifndef _RT_CUDA_PRECISION_H_
#define _RT_CUDA_PRECISION_H_

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <math_functions.h>

/* Redefine math instrinsics to use the fast device functions, if requested.
 * Only real significant impact will be in RT_CUDA_Luminosities.cu, but
 * won't harm other parts of RT_CUDA.
 *
 * Include this header AFTER all other includes. */

#ifdef CUDA_FAST_MATH
#  ifdef EXP
#    undef EXP
#  endif
#  ifdef EXP10
#    undef EXP10
#  endif
#  ifdef POW
#    undef POW
#  endif
#  ifdef SQRT
#    undef SQRT
#  endif
#  ifdef LOG
#    undef LOG
#  endif
#  ifdef LOG10
#    undef LOG10
#  endif
#  ifdef FABS
#    undef FABS
#  endif
#  ifdef FMAX
#    undef FMAX
#  endif
#  ifdef FMIN
#    undef FMIN
#  endif
#  ifdef RT_DOUBLE
#    define EXP __exp
#    define EXP10 __exp10
#    define POW __pow
#    define SQRT __sqrt
#    define LOG __log
#    define LOG10 __log10
#    define FABS __fabs
#    define FMAX __fmax
#    define FMIN __fmin
#  else
#    define EXP __expf
#    define EXP10 __exp10f
#    define POW __powf
#    define SQRT __fsqrt_rn
#    define LOG __logf
#    define LOG10 __log10f
#    define FABS __fabsf
#    define FMAX __fmaxf
#    define FMIN __fminf
#  endif
#endif

#endif
