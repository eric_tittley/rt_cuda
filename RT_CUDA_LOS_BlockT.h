#ifndef _RT_CUDA_LOS_BLOCKT_H_
#define _RT_CUDA_LOS_BLOCKT_H_

struct RT_CUDA_LOS_Block {
  int lo;
  int hi;
  int num_los;
};
typedef struct RT_CUDA_LOS_Block RT_CUDA_LOS_BlockT;

#endif
