/* Sets a structure in host memory with pointers to device memory */

#include <stdlib.h>

#include "RT_CUDA.h"
#include "RT_IntegralInvolatilesT.h"
#include "cudasupport.h"

int RT_CUDA_allocateIntegralInvolatilesOnDevice(
    RT_IntegralInvolatilesT const Involatiles_h,
    RT_IntegralInvolatilesT* const Involatiles_d) {
  Involatiles_d->nFrequencies = Involatiles_h.nFrequencies;
  Involatiles_d->nSpecies = Involatiles_h.nSpecies;

  /* Allocate memory */
  size_t MemoryRequired = Involatiles_h.nFrequencies * sizeof(rt_float);

  cudaMalloc((void**)&(Involatiles_d->Frequencies), MemoryRequired);
  checkCUDAErrors(
      "Unable to allocate device memory for Frequencies", __FILE__, __LINE__);
  cudaMalloc((void**)&(Involatiles_d->Weights), MemoryRequired);
  checkCUDAErrors(
      "Unable to allocate device memory for Weights", __FILE__, __LINE__);
  cudaMalloc((void**)&(Involatiles_d->Luminosities), MemoryRequired);
  checkCUDAErrors(
      "Unable to allocate device memory for Luminosities", __FILE__, __LINE__);
  cudaMalloc((void**)&(Involatiles_d->Alphas),
             Involatiles_h.nSpecies * MemoryRequired);
  checkCUDAErrors(
      "Unable to allocate device memory for Alphas", __FILE__, __LINE__);

  return EXIT_SUCCESS;
}
