/* RT_CUDA_RecombinationCoefficients: Recombination coefficients alpha and beta
 *                                    for H and He.
 *
 * RT_CUDA_RecombinationCoefficients(rt_float const tau_H1_cell,
 *				     rt_float const T,
 *				     rt_float *alpha_H1,
 *				     rt_float *alpha_He1,
 *				     rt_float *alpha_He2,
 *				     rt_float *beta_H1,
 *				     rt_float *beta_He1,
 *				     rt_float *beta_He2 );
 *
 * ARGUMENTS INPUT, NOT MODIFIED
 *  tau_H1_cell		The optical depth in the cell. Only used if
 *			USE_B_RECOMBINATION_CASE set.
 *  T			The temperature [K].
 *
 * ARGUMENTS MODIFIED
 *  alpha_*		Recombination coefficients. [m^3 s^-1]
 *  beta_*		Recombination cooling coefficients. [J m^3 s^-1]
 *
 * PREPROCESSOR MACROS
 *  USE_B_RECOMBINATION_CASE
 *
 * AUTHOR: Eric Tittley
 */
#pragma once
#include <stdlib.h>

#include "RT_CUDA.h"
#include "RecombApprox.h"

#define SET_RETURNS  \
  (*alpha_H1) = 0.;  \
  (*alpha_He1) = 0.; \
  (*alpha_He2) = 0.; \
  (*beta_H1) = 0.;   \
  (*beta_He1) = 0.;  \
  (*beta_He2) = 0.;

inline __device__ void RT_CUDA_RecombinationCoefficients(
    rt_float const tau_H1_cell,
    rt_float const T,
    /*@out@*/ rt_float* const alpha_H1,
    /*@out@*/ rt_float* const alpha_He1,
    /*@out@*/ rt_float* const alpha_He2,
    /*@out@*/ rt_float* const beta_H1,
    /*@out@*/ rt_float* const beta_He1,
    /*@out@*/ rt_float* const beta_He2) {
  enum RecombApprox recomb_approx;

  /*Calculates recombination coefficients m^3 s^-1 */
#ifdef USE_B_RECOMBINATION_CASE_ONLY
  recomb_approx = Approx_B;
#else
#  ifdef USE_B_RECOMBINATION_CASE
  if (tau_H1_cell < 1.0) {
    recomb_approx = Approx_A;
  } else {
    recomb_approx = Approx_B;
  }
#  else
  /* Default if no MACROS defined */
  recomb_approx = Approx_A;
#  endif
#endif

  (*alpha_H1) = RT_CUDA_recomb_H1(T, recomb_approx);
  // if(!isfinite((*alpha_H1)))  (*alpha_H1) =1e-19;
  (*alpha_He1) = RT_CUDA_recomb_He1(T, recomb_approx);
  // if(!isfinite((*alpha_He1))) (*alpha_He1)=1e-19;
  (*alpha_He2) = RT_CUDA_recomb_He2(T, recomb_approx);
  // if(!isfinite((*alpha_He2))) (*alpha_He2)=1e-19;

  /* Calculates cooling recombination coefficient m^3 s^-1 */
  (*beta_H1) = RT_CUDA_recombcool_H1(T, recomb_approx);
  (*beta_He1) = RT_CUDA_recombcool_He1(T, recomb_approx);
  (*beta_He2) = RT_CUDA_recombcool_He2(T, recomb_approx);
}
