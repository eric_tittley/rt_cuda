#include <nppdefs.h>

#include "RT_CUDA.h"
#include "cudasupport.h"

__global__ void sHEV_iterate_kernel(RT_Data const Data,
                                    RT_RatesBlockT const Rates) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock
  size_t cellindex;
  for (cellindex = istart; cellindex < Data.NumCells; cellindex += span) {
    double f_HI, f_HII;
    if (Rates.Gamma_HI[cellindex] < NPP_MINABS_32F) {
      // No ionizing radiation.  Equilibrium is fully neutral
      f_HI = 1.0;
      f_HII = 0.0;
    } else if ((Rates.alpha_H1[cellindex] * Data.n_H[cellindex]) <
               NPP_MINABS_32F) {
      // No recombinations or no hydrogen. Equilibrium is fully ionized
      f_HI = 0.0;
      f_HII = 1.0;
    } else {
      // Analytic solution
      rt_float const n_He2 = Data.n_He[cellindex] * Data.f_He2[cellindex];
      rt_float const n_He3 = Data.n_He[cellindex] * Data.f_He3[cellindex];
      double const n_e_He = n_He2 + 2. * n_He3;

      // Dimensionless variables
      double const rel_e_He = n_e_He / Data.n_H[cellindex];
      double const rel_gamma =
          (double)Rates.Gamma_HI[cellindex] /
          ((double)Rates.alpha_H1[cellindex] * (double)Data.n_H[cellindex]);

      // Solve quadratic for the smaller of f_HI or f_HII
      bool const ionization_dominant = rel_e_He + rel_gamma > 1.;
      if (ionization_dominant) {
        double const b = 2. + rel_e_He + rel_gamma;
        double const c = 1. + rel_e_He;
        double const d = 2. / b;

        // b may be large, so b - sqrt(b^2 - 4c) may be inaccurate.
        // take out a factor of b so everything is small.
        // Also, (1-sqrt(1-x)) = x/2 for x<0.01 which is less prone to underflow
        double const X = c * d * d;
        if (X < 0.01) {
          f_HI = 0.25 * b * X;
        } else {
          f_HI = .5 * b * (1. - __dsqrt_rd(1. - X));
        }
        f_HII = 1. - f_HI;
      } else {
        double const b = rel_e_He + rel_gamma;

        f_HII = .5 * (__dsqrt_rd(b * b + 4. * rel_gamma) - b);
        f_HI = 1. - f_HII;
      }
    }

    Rates.n_HI_Equilibrium[cellindex] = f_HI * Data.n_H[cellindex];
    Rates.n_HII_Equilibrium[cellindex] = f_HII * Data.n_H[cellindex];

    if (Rates.n_HI_Equilibrium[cellindex] > Data.n_H[cellindex]) {
      printf(
          "WARNING: %s: %i: Failed to converge on sensible value. n_H1=%10.3e; "
          "n_H=%10.3e",
          __FILE__,
          __LINE__,
          Rates.n_HI_Equilibrium[cellindex],
          Data.n_H[cellindex]);
      Rates.n_HI_Equilibrium[cellindex] = Data.n_H[cellindex];
      Rates.n_HII_Equilibrium[cellindex] = 0.0;
    }
    if (Rates.n_HII_Equilibrium[cellindex] > Data.n_H[cellindex]) {
      printf(
          "WARNING: %s: %i: Failed to converge on sensible value. n_H2=%10.3e; "
          "n_H=%10.3e",
          __FILE__,
          __LINE__,
          Rates.n_HII_Equilibrium[cellindex],
          Data.n_H[cellindex]);
      Rates.n_HI_Equilibrium[cellindex] = 0.0;
      Rates.n_HII_Equilibrium[cellindex] = Data.n_H[cellindex];
    }

  } /* End loop over cells */
}

__global__ void sHEV_noHE_kernel(RT_Data const Data,
                                 RT_RatesBlockT const Rates) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock
  for (size_t cellindex = istart; cellindex < Data.NumCells;
       cellindex += span) {
    double f_HI, f_HII;
    if (Rates.Gamma_HI[cellindex] < NPP_MINABS_32F) {
      // No ionizing radiation.  Equilibrium is fully neutral
      f_HI = 1.0;
      f_HII = 0.0;
    } else if ((Rates.alpha_H1[cellindex] * Data.n_H[cellindex]) <
               NPP_MINABS_32F) {
      // No recombinations or no hydrogen. Equilibrium is fully ionized
      f_HI = 0.0;
      f_HII = 1.0;
    } else {
      // Analytic solution
      double const rel_gamma =
          (double)Rates.Gamma_HI[cellindex] /
          ((double)Rates.alpha_H1[cellindex] * (double)Data.n_H[cellindex]);

      // Solve quadratic for the smaller of f_HI or f_HII
      bool const ionization_dominant = rel_gamma > 1.;
      if (ionization_dominant) {
        double const b = 2. + rel_gamma;
        double const d = 2. / b;

        // b may be large, so b - sqrt(b^2 - 4) may be inaccurate.
        // take out a factor of b so everything is small.
        // Also, (1-sqrt(1-x)) = x/2 for x<0.01 which is less prone to underflow
        double const X = d * d;
        if (X < 0.01) {
          f_HI = .25 * b * X;
        } else {
          f_HI = .5 * b * (1. - __dsqrt_rd(1. - X));
        }
        f_HII = 1. - f_HI;
      } else {
        f_HII = .5 * (__dsqrt_rd(rel_gamma * (4. + rel_gamma)) - rel_gamma);
        f_HI = 1. - f_HII;
      }
    }

    Rates.n_HI_Equilibrium[cellindex] = f_HI * Data.n_H[cellindex];
    Rates.n_HII_Equilibrium[cellindex] = f_HII * Data.n_H[cellindex];

#ifdef DEBUG
    if ((!(bool)isfinite(Rates.n_HI_Equilibrium[cellindex])) ||
        (Rates.n_HI_Equilibrium[cellindex] < NPP_MINABS_32F)) {
      printf(
          "WARNING: %s: %i: cellindex=%lu; n_HI_Equilibrium=%5.3e; "
          "Gamma_HI=%5.3e; alpha_H1=%5.3e; n_H=%5.3e\n",
          __FILE__,
          __LINE__,
          cellindex,
          Rates.n_HI_Equilibrium[cellindex],
          Rates.Gamma_HI[cellindex],
          Rates.alpha_H1[cellindex],
          Data.n_H[cellindex]);
    }
#endif
  }  // End loop over cells
}

void RT_CUDA_setHydrogenEquilibriumValues(RT_ConstantsT const Constants,
                                          RT_Data const Data,
                                          RT_RatesBlockT const Rates) {
  int gridSize;   // The actual grid size needed, based on input
                  // size
  int blockSize;  // The launch configurator returned block size

  bool const TerminateOnCudaError = true;

  if (Constants.Y > 0.0) {
    setGridAndBlockSize(
        Data.NumCells, (void *)sHEV_iterate_kernel, &gridSize, &blockSize);

    sHEV_iterate_kernel<<<gridSize, blockSize>>>(Data,
                                                 /* out */ Rates);
    checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
  } else {
    /* No helium */
    setGridAndBlockSize(
        Data.NumCells, (void *)sHEV_noHE_kernel, &gridSize, &blockSize);

    sHEV_noHE_kernel<<<gridSize, blockSize>>>(Data,
                                              /* out */ Rates);
    checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
  }
}
