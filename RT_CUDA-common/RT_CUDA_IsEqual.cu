/* isequal: f_isequal tests the equivalence of two floats.
 *          d_isequal tests the equivalence of two rt_floats.
 *
 * ARGUMENTS
 *  n, d    Two numbers to compare.
 *
 * RETURNS
 *  TRUE if n is equal to d to within machine precision.
 *  FALSE if n is not equal to d to withing machine precision.
 *
 * AUTHOR: Eric Tittley
 */

#include "Epsilon.h"
#include "RT_CUDA.h"

__device__ bool RT_CUDA_isequal(rt_float const n, rt_float const d) {
  if ((d < 1.0) && (n > d * FLOAT_MAX)) return FALSE;
  if (((1.0 - EPSILON) < (n / d)) && ((n / d) < (1.0 + EPSILON))) {
    return TRUE;
  } else {
    return FALSE;
  }
}
