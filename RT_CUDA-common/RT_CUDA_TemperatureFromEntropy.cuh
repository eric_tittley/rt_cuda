#pragma once

#include <cuda_runtime.h>

#include "RT_CUDA.h"

inline __device__ rt_float
RT_CUDA_TemperatureFromEntropy(rt_float const Entropy,
                               size_t const iCell,
                               RT_Data const* const Data,
                               RT_ConstantsT const* const Constants) {
  /* Update the neutral & ionised number densities. */
  rt_float const n_H2 = Data->n_H[iCell] * Data->f_H2[iCell];
  rt_float const n_He2 = Data->n_He[iCell] * Data->f_He2[iCell];
  rt_float const n_He3 = Data->n_He[iCell] * Data->f_He3[iCell];

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2. * n_He3);

  /* Total number density */
  rt_float const n = Data->n_H[iCell] + Data->n_He[iCell] + n_e;

  rt_float const mu = (Data->n_H[iCell] + 4. * Data->n_He[iCell]) / n;

  /* Note the scaling of the density
   * 10^( 0 + (-27 - -23) + 22 -2/3*22 + (2/3)*(-28+22) )
   * 10^(     -3          + 7          - 4)
   * 10^( 0 )
   * 1
   */
  rt_float const Temperature =
      mu * (Constants->mass_p / Constants->k_B) * Entropy *
      POW(Constants->Mpc, 1.0 - Constants->gamma_ad) *
      POW(Data->Density[iCell] * Constants->Mpc, Constants->gamma_ad - 1.0);
#ifdef DEBUG
  if (!(bool)isfinite(Temperature)) {
    printf(
        "ERROR: %s: %i: Temperature is out of range: %e. Entropy=%5.3e; "
        "Density=%5.3e\n",
        __FILE__,
        __LINE__,
        Temperature,
        Entropy,
        Data->Density[iCell]);
  }
#endif
  return Temperature;
}
