#include "RT_CUDA.h"

__device__ void RT_CUDA_ConstantsPrint(RT_ConstantsT const* const Constants) {
  printf("Constants.NLevels (%p)=(%lu %lu %lu)\n",
         Constants->NLevels,
         Constants->NLevels[0],
         Constants->NLevels[1],
         Constants->NLevels[2]);
}
