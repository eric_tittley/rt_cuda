/* RT_CUDA_PhotoionizationRates
 *  Evaluate the photoionization and photoionization heating rates.
 *
 * RT_CUDA_PhotoionizationRates(rt_float const N_H1,
 *                              rt_float const N_He1,
 *                              rt_float const N_He2,
 *                              rt_float const N_H1_cum,
 *                              rt_float const N_He1_cum,
 *                              rt_float const N_He2_cum,
 *                              rt_float const RedshiftOfSource,
 *                              RT_SourceT const Source,
 *                              RT_ConstantsT const Constants,
 *                              rt_float const R,
 *                              rt_float const dR,
 *                              rt_float *I_H1,
 *                              rt_float *I_He1,
 *                              rt_float *I_He2,
 *                              rt_float *G_H1,
 *                              rt_float *G_He1,
 *                              rt_float *G_He2,
 *                              rt_float *Gamma_HI)
 *
 * ARGUMENTS
 *  Input, not modified
 *   N_H1, N_He1, N_He2                   Column densities in the cell. [m^2]
 *   N_H1_cum,N_He1_cum,N_He2_cum        Cumulative column densities between
 *   RedshiftOfSource                       Redshift of the source (used if the
 * source is a function of time). Source                        The Source
 *   Constants                     Physical constants
 *   R                             The distance to the source [m]
 *   dR                            The depth through the cell or particle [m]
 *
 *  Output
 *   I_H1, I_He1, I_He2                   photoionization rates [s^-1]
 *   G_H1, G_He1, G_He2                   photoionization heating rates [W]
 *   Gamma_HI                                      photoionization rate per
 * particle [m^3 s^-1 Hz^-1
 *
 * NOTES
 *
 * CALLS
 *  RT_CUDA_PhotoionizationRatesIntegrate() which worries about the method used
 *  and splits the integral into three parts.
 *
 * AUTHOR: Eric Tittley
 */
#pragma once
#include <stdlib.h>

#include "RT_CUDA.h"
#ifdef DEBUG
#  include <nppdefs.h>
#  include <stdio.h>
#endif
/* Currently need "gcc -D_GNU_SOURCE --finite-math-only" to get exp10() */
#include <cuda_runtime.h>

inline __device__ RT_CUDA_PhotoRates RT_CUDA_PhotoionizationRates(
    rt_float const N_H1,
    rt_float const N_He1,
    rt_float const N_He2,
    rt_float const N_H1_cum,
    rt_float const N_He1_cum,
    rt_float const N_He2_cum,
    rt_float const ExpansionFactor,
    RT_ConstantsT const* const Constants,
    RT_SourceT const* const Source,
    rt_float const DistanceFromSourceMpc,
    rt_float const ParticleRadiusMpc,
    rt_float const DistanceThroughElementMpc,
    RT_IntegralInvolatilesT const* const Involatiles_dev) {
  auto Rates = RT_CUDA_PhotoionizationRatesIntegrate(N_H1,
                                                     N_He1,
                                                     N_He2,
                                                     N_H1_cum,
                                                     N_He1_cum,
                                                     N_He2_cum,
                                                     Constants,
                                                     Involatiles_dev);

  return RT_CUDA_Attenuate(Constants,
                           Source,
                           ExpansionFactor,
                           DistanceFromSourceMpc,
                           DistanceThroughElementMpc,
                           ParticleRadiusMpc,
                           Rates);
}
