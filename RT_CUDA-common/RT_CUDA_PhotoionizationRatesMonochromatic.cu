/* PhotoionizationRatesMonochromatic: Evaluate the photoionization and
 * photoionization heating rate at a specific frequency.
 *
 * int PhotoionizationRatesMonochromatic(rt_float N_H1, rt_float N_He1, rt_float
 * N_He2, rt_float N_H1_cum, rt_float N_He1_cum, rt_float N_He2_cum, rt_float z,
 * rt_float f[6])
 *
 * ARGUMENTS
 *  Input, not modified
 *   N_H1, N_He1, N_He2         Column densities in the cell. [m^2]
 *   N_H1_cum, N_He1_cum, N_He2_cum     Cumulative column densities between
 *   z          Redshift (used if the source is a function of time).
 *
 *  Output
 *   f[7]       Rates at frequency, nu. [I_H1, I_He1, I_He2, G_H1, G_He1, G_He2]
 *              I_* are the photoionisation Rates (* h_p). [s^-1]
 *              G_* are the heating Rates. [J s^-1]
 *              Gamma_HI* the equilibrium photoionization rate
 *
 * AUTHOR: Eric Tittley
 */

#include "RT_CUDA.h"

extern "C" {
#include "RT_IntegralInvolatilesT.h"
}

#ifdef DEBUG
#  include <nppdefs.h>
#endif

/**********************************************************************
 * Integration via parts: nu_0 to nu_1, nu_1 to nu_2, nu_2 to nu_end. *
 * This assists convergence since it avoid the discontinuities at     *
 * these frequencies.                                                 *
 **********************************************************************/
__device__ RT_CUDA_PhotoRates RT_CUDA_PhotoionizationRatesMonochromatic(
    rt_float const N_H1,
    rt_float const N_He1,
    rt_float const N_He2,
    rt_float const N_H1_cum,
    rt_float const N_He1_cum,
    rt_float const N_He2_cum,
    RT_ConstantsT const* const Constants,
    RT_IntegralInvolatilesT const* const Involatiles) {
  auto Rates =
      RT_CUDA_PhotoionizationRatesFunctions(Involatiles->Luminosities[0],
                                            Involatiles->Luminosities[0],
                                            N_H1,
                                            N_He1,
                                            N_He2,
                                            N_H1_cum,
                                            N_He1_cum,
                                            N_He2_cum,
                                            Constants,
                                            Involatiles->Alphas);

#ifdef DEBUG
  rt_float f[7] = {Rates.I_H1,
                   Rates.I_He1,
                   Rates.I_He2,
                   Rates.G_H1,
                   Rates.G_He1,
                   Rates.G_He2,
                   Rates.Gamma_HI};
  for (size_t i = 0; i < 7; i++) {
    if (!isfinite(f[i]) || (f[i] < -NPP_MINABS_32F)) {
      printf("%s: %i: ERROR: f[%lu]=%e\n", __FILE__, __LINE__, i, f[i]);
      return Rates;
    }
  }
#endif

  return Rates;
}
