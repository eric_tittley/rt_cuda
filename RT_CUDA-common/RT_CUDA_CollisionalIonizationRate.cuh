/*
 * Calcualtes the collisional ionization rate by electrons with a Maxwellian
 * velocity distribution. Either from "Spectral line formation, Jefferies, John
 * T. (1968)" which is includes Helium rates but is valid for a smaller range of
 * temperatures. Its is based on approximating the interaction crossection using
 * a dipole approximation.
 *
 * Alternatively we can use the form given in Schulz & Walters' '91 which is
 * used in RT_CUDA_CoolingRatesExtra to calculate cooling due to collisional
 * exitation. This is valid for a wider range of temperatures and is based on a
 * combination of theoretical models and observed values from experiments. It
 * should be more accurate. Note: this only includes hydrogen and only
 * ionizations from the ground state. There should be additional terms detailing
 * the collisional ionization of excited states.
 *
 * Each collisional ionization effectively removes h_p*nu_0 Joules from the gas.
 * This gets added to the cooling rate.
 *
 */
#pragma once

#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <nppdefs.h>
#  include <stdio.h>
#endif

#include "RT_CUDA.h"

inline __device__ void RT_CUDA_CollisionalIonizationRate(
    RT_ConstantsT const* const Constants,
    size_t const cell,
    rt_float const alpha_H1,
    rt_float const alpha_He1,
    rt_float const alpha_He2,
    RT_Data const* const Data,
    RT_RatesBlockT* Rates) {
  /* Find the neutral & ionised number densities. */
  rt_float const n_H1 = Data->n_H[cell] * Data->f_H1[cell];
  rt_float const n_H2 = Data->n_H[cell] * Data->f_H2[cell];
  rt_float const n_He2 = Data->n_He[cell] * Data->f_He2[cell];
  rt_float const n_He3 = Data->n_He[cell] * Data->f_He3[cell];

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);
  rt_float const T = RT_CUDA_TemperatureFromEntropy(
      Data->Entropy[cell], cell, Data, Constants);

  /* a_XX = ionization potential / kT */
  // h_p/k_B is the same in cgs and mks
  rt_float const a_H1 =
      (Constants->h_p * Constants->nu_0) / (Constants->k_B * T);

  rt_float constexpr CICoef[7] = {-9.61443e+1,
                                  3.79523e+1,
                                  -7.96855e+0,
                                  8.83922e-1,
                                  -5.34513e-2,
                                  1.66344e-3,
                                  -2.08888e-5};

  rt_float const lnT1 = LOG(T);
  rt_float const lnT2 = lnT1 * lnT1;
  rt_float const lnT3 = lnT1 * lnT2;
  rt_float const lnT4 = lnT1 * lnT3;
  rt_float const lnT5 = lnT1 * lnT4;
  rt_float const lnT6 = lnT1 * lnT5;
  rt_float const exponent = CICoef[0] + CICoef[1] * lnT1 + CICoef[2] * lnT2 +
                            CICoef[3] * lnT3 + CICoef[4] * lnT4 +
                            CICoef[5] * lnT5 + CICoef[6] * lnT6;
  rt_float const gamma = EXP(exponent) * EXP(-a_H1);
  // gamma should have units cm^3 s^-1
  // factor of 1e-6 is conversion from cm^3 to m^3
  rt_float const rate_H1 = gamma * n_H1 * n_e * RT_FLT_C(1e-6);
  Rates->I_H1[cell]     += rate_H1;
  Rates->Gamma_HI[cell] += gamma * n_e * RT_FLT_C(1e-6);
  //need to scale my Ma as cooling rate is scaled by it to make it fit into float range
  //This cooling is already accounted for in RT_CUDA_CoolingRatesExtra so only need to
  //account for it if NO_COOLING is enabled.
  #ifdef NO_COOLING
  Rates->L[cell] += rate_H1 * Constants->h_p * Constants->nu_0 * Constants->Ma;
  #endif
}
