/* RT_CUDA_CopyConstantsToDevice: Allocate and copy RT_ConstantsT array data to
 * device
 *
 * int RT_CUDA_CopyConstantsToDevice(RT_ConstantsT const Constants,
 *                                   RT_ConstantsT * Constants_dev);
 *
 * ARGUMENT
 *  Constants           RT_ConstantsT structure with array data to copy to
 * device Constants_dev       Copy of Constants in which to replace the pointers
 * with pointers to device memory.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *
 * USAGE
 *
 * RT_ConstantsT Constants_dev = Constants;
 * RT_CUDA_CopyConstantsToDevice(Constants, Constants_dev);
 *
 * Then pass Constants_dev to the kernel.
 *
 * SEE ALSO
 *  RT_CUDA_FreeConstantsOnDevice
 *
 * AUTHOR: Eric Tittley
 */

#include <cuda.h>
#include <cuda_runtime.h>
#include <driver_types.h>
#include <stdio.h>
#include <stdlib.h>

#include "RT_CUDA.h"

int RT_CUDA_CopyConstantsToDevice(RT_ConstantsT const Constants,
                                  RT_ConstantsT *Constants_dev) {
  /* Allocate memory on the CUDA device for the arrays in Constants
   * then copy the contents to the device. */

  /* NLevels */
  cudaMalloc((void **)(&(Constants_dev->NLevels)),
             sizeof(size_t) * Constants.NLevels_NIntervals);
  cudaMemcpy(Constants_dev->NLevels,
             Constants.NLevels,
             sizeof(size_t) * Constants.NLevels_NIntervals,
             cudaMemcpyHostToDevice);

  return EXIT_SUCCESS;
}
