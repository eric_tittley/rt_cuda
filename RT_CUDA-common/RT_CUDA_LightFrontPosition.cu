#include <cuda_runtime.h>

#include "RT_CUDA.h"

/* The rate of change of the expansion factor, a, over time, t
 * See notes 080214 */
__device__ rt_float RT_CUDA_LFP_dadt(rt_float const t,
                                     RT_ConstantsT const* const Constants) {
  rt_float const H_0 = Constants->h * 1e5 / Constants->Mpc; /* s^{-1} */
  rt_float T1, T2, T3, T4, T5;
  T1 = CBRT(Constants->omega_m / (1. - Constants->omega_m));
  T2 = SQRT(1. - Constants->omega_m);
  T3 = COSH(3. / 2. * H_0 * T2 * t);
  T4 = SINH(3. / 2. * H_0 * T2 * t);
  T5 = CBRT(T4);
  return T1 * T3 / T5 * H_0 * T2;
  /* The following is an approximation valid for t << 1/H_0. Keep for testing.
   */
  /*
     return POW(2./3.*omega_m*H_0*H_0/t,1./3.);
   */
}

/* Integrate to determine the position of the light front
 *
 * ARGUMENTS
 *  t_o	Time the source turned on
 *  t   Time at which to determine the position
 *  dt	The time stepping. Used only as an estimate. The actual dt used will
 *	fit into the interval (t-t_o) an integer number of times.
 *
 * RETURNS
 *  distance of light from from source after the time (t-t_o) [m]
 */
__device__ rt_float RT_CUDA_LFP_integral(rt_float const t_o,
                                         rt_float const t,
                                         rt_float dt,
                                         RT_ConstantsT const* const Constants) {
  rt_float r = 0.0;
  rt_float tau;
  int i, N;

  /* Set dt to fit into the interval an integer number of times.
   * N is the number of intervals. */
  N = (int)((t - t_o) / dt + 0.4);
  if (N < 1) {
    printf(
        "WARNING: RT_LFP_integral: dt too large.  Setting to the interval.\n");
    printf("         t_o=%5.3e; t=%5.3e; dt=%5.3e\n", t_o, t, dt);
    N = 1;
  }
  dt = (t - t_o) / ((rt_float)N);

  for (i = 0; i < N; i++) {
    tau = t_o + (rt_float)i * dt;
    r = r + (r * RT_CUDA_LFP_dadt(tau, Constants) + Constants->c) * dt;
  }
  return r;
}

/* Determine the position of the light front
 *
 * ARGUMENTS
 *  t_o		Time the source turned on
 *  t   	Time at which to determine the position
 *  delta_r	Tolerance to which to calculate r.
 *
 * RETURNS
 *  distance of light from from source [m]
 */
__device__ rt_float
RT_CUDA_LFP_LightFrontPosition(rt_float const t_o,
                               rt_float const t,
                               rt_float const delta_r,
                               RT_ConstantsT const* const Constants) {
  rt_float r = 2.0 * delta_r;
  rt_float r_old = 0;
  rt_float dt = (t - t_o) / 16;

  while (fabs(r_old - r) > delta_r) {
    r_old = r;
    r = RT_CUDA_LFP_integral(t_o, t, dt, Constants);
    dt = dt / 2.;
  }
  return r;
}

__device__ rt_float
RT_CUDA_LightFrontPosition(rt_float const t,
                           rt_float const t_on,
                           rt_float const DistanceTolerance,
                           RT_ConstantsT const* const Constants) {
  rt_float R_light_front; /* Position of light front when timestep starts */

  /* The position of the light front at the begining of the time interval,
   * dt_RT. */
#ifndef NO_EXPANSION
  /* The above is an approximation if there is no expansion.  Expansion carries
   * the light front further.
   * Cosmological expansion modifies the position of the light front by < 10% */
  R_light_front =
      RT_CUDA_LFP_LightFrontPosition(t_on, t, DistanceTolerance, Constants);
#else
  R_light_front = Constants->c * (t - t_on); /* m */
#endif

  return R_light_front;
}
