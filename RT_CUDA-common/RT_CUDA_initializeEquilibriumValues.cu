#include "RT_CUDA.h"
#include "cudasupport.h"

__global__ void RC_iEV_kernel(size_t const N,
                              rt_float* const F1,
                              rt_float* const F2,
                              /*@out@*/ rt_float* const P) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t cellindex = istart; cellindex < N; cellindex += span) {
    P[cellindex] = F1[cellindex] * F2[cellindex];
  }
}

void RT_CUDA_initializeEquilibriumValues(RT_Data const Data,
                                         RT_RatesBlockT Rates) {
  int blockSize;  // The launch configurator returned block size
  int gridSize;   // The actual grid size needed, based on input size
  setGridAndBlockSize(
      Data.NumCells, (void*)RC_iEV_kernel, &gridSize, &blockSize);

  bool TerminateOnCudaError = true;

  RC_iEV_kernel<<<gridSize, blockSize>>>(Data.NumCells,
                                         Data.f_H1,
                                         Data.n_H,
                                         /*@out@*/ Rates.n_HI_Equilibrium);

  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);

  RC_iEV_kernel<<<gridSize, blockSize>>>(Data.NumCells,
                                         Data.f_H2,
                                         Data.n_H,
                                         /*@out@*/ Rates.n_HII_Equilibrium);

  checkCudaKernelLaunch(__FILE__, __LINE__, TerminateOnCudaError);
}
