#pragma once
#include "RT_CUDA.h"

inline __device__ bool RT_CUDA_useEquilibriumValues(
    size_t const cellindex,
    RT_Data const* const Data,
    RT_RatesBlockT const* const Rates,
    RT_ConstantsT const* const Constants) {
/* If
 *  H is mostly ionized
 *   OR
 *  element is mostly ionised AND HI is within Tolerance of equilibrium
 *   OR
 *  element is mostly neutral AND HII is within Tolerance of equilibrium
 * then set to the equilibrium value.
 */

 rt_float const Tolerance = 0.01;
 rt_float const n_H1 = Data->n_H[cellindex] * Data->f_H1[cellindex];
 rt_float const n_H2 = Data->n_H[cellindex] * Data->f_H2[cellindex];
 /* The following tests can suffer a divide-by-zero but the first condition
  * will override those */
#define HI_TRIGGER
#define HII_TRIGGER
 return (
        (Data->f_H1[cellindex] < Constants->xHIEquilibriumThreshold)
#ifdef HI_TRIGGER
     || (    (FABS(n_H1 / Rates->n_HI_Equilibrium[cellindex] - 1.0) < Tolerance)
          && (Data->f_H1[cellindex] < 0.5) )
#endif
#ifdef HII_TRIGGER
      || (   (FABS(n_H2 / Rates->n_HII_Equilibrium[cellindex] - 1.0) < Tolerance)
           && (Data->f_H2[cellindex] < 0.5) )
#endif
  );
}
