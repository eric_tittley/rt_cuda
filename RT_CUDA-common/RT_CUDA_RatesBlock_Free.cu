/*
 * ARGUMENTS
 *  Rates_dev
 */
#include <cuda.h>
#include <stdio.h>

#include "RT_CUDA.h"

void RT_CUDA_RatesBlock_Free(RT_RatesBlockT Rates_dev) {
  cudaError_t error;
  size_t nParticles = Rates_dev.NumCells;
  error = cudaFree((Rates_dev.I_H1));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.I_He1));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.I_He2));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.G));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.G_H1));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.G_He1));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.G_He2));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.alpha_H1));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.alpha_He1));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.alpha_He2));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.L));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.Gamma_HI));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.n_HI_Equilibrium));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.n_HII_Equilibrium));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
  error = cudaFree((Rates_dev.TimeScale));
  if (error != cudaSuccess) {
    printf("ERROR: %s: %i: ERROR: Unable to free %lu bytes on CUDA device\n",
           __FILE__,
           __LINE__,
           nParticles * sizeof(rt_float));
  }
}
