/* cosmological_time: Age of the universe given the expansion factor.
 *
 * rt_float cosmological_time(rt_float a)
 *
 * ARGUMENTS
 *  Input, not modified
 *   a	The expansion factor at which point to find the age.
 *
 * RETURNS
 *   The age of the universe. [s]
 *
 * AUTHOR: Eric Tittley
 */

#include "RT_CUDA.h"

#ifdef DEBUG
#  include <stdio.h>
#endif
#include <cuda_runtime.h>
#ifdef S_SPLINT_S
extern rt_float ASINH(rt_float);
extern int isfinite(rt_float);
#endif

__device__ rt_float
RT_CUDA_CosmologicalTime(rt_float const a,
                         RT_ConstantsT const* const Constants) {
  /* Time (s) as a function of expansion factor */
  /* Note, this equation reduces to  2/(3H_0) * a^1.5 for omega_m=1 */
  rt_float const H_0 = Constants->h * 1e5 / Constants->Mpc; /* s^{-1} */
  rt_float ct, ct1, ct2, ct3;
  if (RT_CUDA_isequal(Constants->omega_m, 1.0)) {
    ct = 2. / (3. * H_0) * POW(a, 1.5);
  } else {
    ct1 = (2. / (3. * H_0 * SQRT(1. - Constants->omega_m)));
    ct2 = a * SQRT(a);
    ct3 = ASINH(SQRT((1. - Constants->omega_m) / Constants->omega_m) * ct2);
    ct = ct1 * ct3;
  }
  return ct;
}
