#pragma once

#include <nppdefs.h>

#include "RT_CUDA.h"

inline __device__ RT_CUDA_PhotoRates
RT_CUDA_Attenuate(RT_ConstantsT const *const Constants,
                  RT_SourceT const *const Source,
                  rt_float const ExpansionFactor,
                  rt_float const DistanceFromSourceMpc,
                  rt_float const DistanceThroughElementMpc,
                  rt_float const ParticleRadiusMpc,
                  /* Input and Output */
                  RT_CUDA_PhotoRates Rates) {
  /* AttenuationFactor is
   * (attenuation of source luminosity)^-1 * ( path length through cell)
   * = area of shell * depth
   * = volume of shell (only incidentally)
   * = (4 pi R^2) * dR
   * where R is the distance to the cell.
   * This assumes a non-plane-wave approximation.

   * For the plane-wave approximation, AttenuationFactor = ( 1/(4 pi R_0^2) ) *
   dR
   */
  rt_float AttenuationFactor = -1.;
  rt_float AttenuationFactorGamma = -1.;
  rt_float theta = -1;
  rt_float ratio = -1;
  /* The following should set the current expansion factor, not the expansion
   * factor of the source as seen from the current location. The difference is
   * small
   */
  switch (Source->AttenuationGeometry) {
    case PlaneWave:
/* Assuming:
 *  plane-wave geometry
 *  Data[0]->R[0] = (R_0 * Mpc)  or  (R_0 * ExpansionFactor * Mpc / h)
 */
#if defined(CONSTANT_DENSITY) && !defined(CONSTANT_DENSITY_COMOVING)
      AttenuationFactorGamma = RT_FLT_C(4.) * RT_PI * Source->R_0 * Source->R_0;
#else
      AttenuationFactorGamma =
          RT_FLT_C(4.) * RT_PI *
          POW(Source->R_0 * ExpansionFactor / Constants->h, RT_FLT_C(2.));
#endif
      AttenuationFactor =
          AttenuationFactorGamma * DistanceThroughElementMpc; /* Mpc^-3 */
      break;
    case SphericalWave: {
      /* Assuming spherical geometry */
      AttenuationFactorGamma =
          RT_FLT_C(4.) * RT_PI * DistanceFromSourceMpc * DistanceFromSourceMpc;
#define CorrectNearFieldAttenuationFactor
#ifdef CorrectNearFieldAttenuationFactor
      /* The Near and Far-field corrections are not continuous at the transition
       * but are within about 10%, which is acceptable: 2.09 h^2 = 1.94 h^2 */
      rt_float r_over_h = DistanceFromSourceMpc / ParticleRadiusMpc;
      if (r_over_h >= RT_FLT_C(1.)) {
        AttenuationFactorGamma *=
            POW(RT_FLT_C(1.5), -POW(r_over_h, RT_FLT_C(-3.0)));
      } else {
        AttenuationFactorGamma = ParticleRadiusMpc * ParticleRadiusMpc;
        AttenuationFactorGamma /=
            RT_FLT_C(1.) / ((RT_FLT_C(4.) / RT_FLT_C(3.)) * RT_PI) +
            RT_FLT_C(0.04) * r_over_h - RT_FLT_C(0.15) * r_over_h * r_over_h;
      }
#endif
      AttenuationFactor =
          AttenuationFactorGamma * DistanceThroughElementMpc; /* Mpc^-3 */
      if ((!(bool)isfinite(AttenuationFactor)) ||
          (AttenuationFactor <= NPP_MINABS_32F)) {
        printf(
            "ERROR: %s: %i: AttenuationFactor=%5.3e: "
            "AttenuationFactorGamma=%5.3e; DistanceFromSourceMpc=%5.3e; "
            "ParticleRadiusMpc=%5.3e; DistanceThroughElementMpc=%5.3e\n",
            __FILE__,
            __LINE__,
            AttenuationFactorGamma,
            AttenuationFactor,
            DistanceFromSourceMpc,
            ParticleRadiusMpc,
            DistanceThroughElementMpc);
      }
      break;
    }
    /* For Ray*, the source luminosity MUST ALREADY BE ATTENUATED by
     * 1/(number of rays) */
    case RayPlaneWave:
      AttenuationFactorGamma = RT_PI * ParticleRadiusMpc * ParticleRadiusMpc;
      AttenuationFactor =
          AttenuationFactorGamma * DistanceThroughElementMpc; /* Mpc^-3 */
      break;
    case RaySphericalWave:
      if (DistanceFromSourceMpc <= ParticleRadiusMpc) {
        // there's a 4PI and a 1/2 in here
        AttenuationFactorGamma = RT_FLT_C(2.) * RT_PI * DistanceFromSourceMpc *
                                 DistanceFromSourceMpc;
      } else {
        // This approximates to radius^2/4*distance^2 for large distances
        // need distance>=10*radius for 1% accuracy in the approximation
        theta =
            (RT_PI * 0.5) - acosf(ParticleRadiusMpc / DistanceFromSourceMpc);
        ratio = sinf(theta * 0.5);
        AttenuationFactorGamma = RT_FLT_C(4.) * RT_PI * DistanceFromSourceMpc *
                                 DistanceFromSourceMpc * ratio * ratio;
      }

      AttenuationFactor =
          AttenuationFactorGamma * DistanceThroughElementMpc; /* Mpc^-3 */
      break;
#ifdef DEBUG
    default:
      printf("ERROR: %s: Unknown Attenuation Geometry: %i\n",
             __FILE__,
             Source->AttenuationGeometry);
      return Rates;
#endif
  }
#ifdef DEBUG
  if ((!(bool)isfinite(AttenuationFactor)) ||
      (AttenuationFactor <= NPP_MINABS_32F)) {
    printf(
        "ERROR: %s: %i: AttenuationFactor=%5.3e: "
        "AttenuationGeometry=%i; "
        "AttenuationFactorGamma=%5.3e; "
        "ParticleRadiusMpc=%5.3e; "
        "DistanceThroughElementMpc=%5.3e; "
        "DistanceFromSourceMpc=%5.3e;\n",
        __FILE__,
        __LINE__,
        AttenuationFactor,
        Source->AttenuationGeometry,
        AttenuationFactorGamma,
        ParticleRadiusMpc,
        DistanceThroughElementMpc,
        DistanceFromSourceMpc);
    return Rates;
  }
#endif

  /* MISSING the Constants->Mpc^-3 factor in the following.
   * This implies that f *must* be too large, too.
   * f is 10^14, h_p is 10-34, AttenuationFactor is 10^4, missing (10^22)^3
   * So we have 10^(14 + 34 - 4 - 66) = 10^(-22)
   * which is entirely doable in float so long as we spread out that 10^22
   * properly. Try: 10^( (14-22) + (34-2*22) -4 ) = 10^(-8 - 10 - 4)*/
  rt_float const IonizationFactor =
      (RT_FLT_C(1.) / Constants->Mpc) /
      ((Constants->h_p * Constants->Mpc) * Constants->Mpc) / AttenuationFactor;
  Rates.I_H1 *= IonizationFactor;
  Rates.I_He1 *= IonizationFactor;
  Rates.I_He2 *= IonizationFactor;
  /* The following scales as 10^(29 -4 -3*22) = 10^-41 so won't fit in a float.
   * Multiply by Ma (1e13) to shift it to 10^(-28) */
  rt_float const Bias = 1.0e35;
  rt_float const HeatingFactor = ((Bias / Constants->Mpc) / Constants->Mpc) *
                                 (Constants->Ma / Constants->Mpc) /
                                 AttenuationFactor; /* Mpc */
  Rates.G_H1 *= HeatingFactor;
  Rates.G_H1 /= Bias;
  Rates.G_He1 *= HeatingFactor;
  Rates.G_He1 /= Bias;
  Rates.G_He2 *= HeatingFactor;
  Rates.G_He2 /= Bias;

  rt_float const GammaFactor = (RT_FLT_C(1.) / Constants->Mpc) /
                               (Constants->h_p * Constants->Mpc) /
                               AttenuationFactorGamma;
  Rates.Gamma_HI *= GammaFactor;

  /* THE HEATING RATES G_* are too large by Ma. SEE UpdateCell() */

#ifdef DEBUG
  /* For debugging */
  if ((Rates.I_H1 < -NPP_MINABS_32F) || (!isfinite(Rates.I_H1))) {
    printf("%s: %i: ERROR: I_H1=%5.3e; h_p=%5.3e; AttenuationFactor=%5.3e\n",
           __FILE__,
           __LINE__,
           Rates.I_H1,
           Constants->h_p,
           AttenuationFactor);
    return Rates;
  }
  if ((Rates.I_He1 < -NPP_MINABS_32F) || (!isfinite(Rates.I_He1))) {
    printf("%s: %i: ERROR: I_He1=%5.3e\n", __FILE__, __LINE__, Rates.I_He1);
    return Rates;
  }
  if ((Rates.I_He2 < -NPP_MINABS_32F) || (!isfinite(Rates.I_He2))) {
    printf("%s: %i: ERROR: I_He2=%5.3e\n", __FILE__, __LINE__, Rates.I_He2);
    return Rates;
  }
  if ((Rates.G_H1 < -NPP_MINABS_32F) || (!isfinite(Rates.G_H1))) {
    printf("%s: %i: ERROR: G_H1=%5.3e\n", __FILE__, __LINE__, Rates.G_H1);
    return Rates;
  }
  if ((Rates.G_He1 < -NPP_MINABS_32F) || (!isfinite(Rates.G_He1))) {
    printf("%s: %i: ERROR: G_He1=%5.3e\n", __FILE__, __LINE__, Rates.G_He1);
    return Rates;
  }
  if ((Rates.G_He2 < -NPP_MINABS_32F) || (!isfinite(Rates.G_He2))) {
    printf("%s: %i: ERROR: G_He2=%e\n", __FILE__, __LINE__, Rates.G_He2);
    return Rates;
  }
  if ((Rates.Gamma_HI < RT_FLT_C(0.)) || (!isfinite(Rates.Gamma_HI))) {
    printf("%s: %i: ERROR: Gamma_HI=%e\n", __FILE__, __LINE__, Rates.Gamma_HI);
    return Rates;
  }
#endif
  return Rates;
}
