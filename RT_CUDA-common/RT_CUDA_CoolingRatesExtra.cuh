#pragma once
/* From CUDA */
#include <cuda_runtime.h>

#include "RT_CUDA.h"

#ifdef DEBUG
#  include <nppdefs.h>
#endif

inline __device__ void RT_CUDA_CoolingRatesExtra(
    RT_ConstantsT const* const Constants,
    size_t const cellindex,
    rt_float const ExpansionFactor,
    RT_Data const* const Data_dev,
    /*@out@*/ RT_RatesBlockT* const Rates_dev) {
#ifndef NO_COOLING
  rt_float const n_H1 = Data_dev->n_H[cellindex] * Data_dev->f_H1[cellindex];
  rt_float const n_H2 = Data_dev->n_H[cellindex] * Data_dev->f_H2[cellindex];
  rt_float const n_He2 = Data_dev->n_He[cellindex] * Data_dev->f_He2[cellindex];
  rt_float const n_He3 = Data_dev->n_He[cellindex] * Data_dev->f_He3[cellindex];

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);

  rt_float const Temperature = RT_CUDA_TemperatureFromEntropy(
      Data_dev->Entropy[cellindex], cellindex, Data_dev, Constants);

  rt_float L_local = 0.0;
#  ifdef DEBUG
  if ((!(bool)isfinite(Temperature)) || (Temperature < NPP_MINABS_32F)) {
    printf("ERROR: %s: %i: Temperature=%e: Entropy=%5.3e\n",
           __FILE__,
           __LINE__,
           Temperature,
           Data_dev->Entropy[cellindex]);
  }
#  endif

  /* Cooling via Collisional Excitation of Neutral Hydrogen
   * L_eH: the cooling rate due to collisional excitation of the excited
   *       levels in neutral hydrogen J m^-3 s^-1.
   *       Spitzer 1978, Physical Processes in the ISM, p.140
   *        which is a fit to Dalgarno & McCray (ARAA 1972, p 375) Table 2
   *        for 3000 to 7500 K.
   *        D&M's Table 2 comes from Gould & Thakur (1970, Ann Phys 61:351)
   *        which I can't get a copy of.
   *       Scholz & Waters '91 (ApJ 380 302) provide a different derivation
   *        that spans a larger range in temperature.
   *
   */
#  ifdef COOLING_COLLISIONAL_EXCITATION_OF_HYDROGEN__SPITZER
  /* n_e = 10^0 to 10^4
   * n_H1 = 10^-5 to 10^5
   * The exponential = 10^-10 to 10^0
   * So we have 10^[ -32 +0 -5 -10 ] = 10^-47
   *         to 10^[ -32 +4 +5 +0  ] = 10^-23
   * The smallest value of a float is 1.17e-38, so we are flirting with
   * underflow.  Scale by Ma
   */
  L_local +=
      (Constants->Ma * 7.3e-32) * n_e * n_H1 * EXP(-118400.0 / Temperature);
#  else
  /* Coefficients for Schulz & Walters' '91 approximation for
   * Cooling via collisional excitation of neutral hydrogen 
   * NOTE: This appears to be the combined cooling rate from both
   * collisional excitations and ionizations.*/
  rt_float const CECoef_lo[6] = {2.137913e+2,
                                 -1.139492e+2,
                                 2.506062e+1,
                                 -2.762755,
                                 1.515352e-1,
                                 -3.290382e-3};
  rt_float const CECoef_hi[6] = {2.7125446e+2,
                                 -9.8019455e+1,
                                 1.40072760e+1,
                                 -9.7808421e-1,
                                 3.3562891e-2,
                                 -4.55332321e-4};
  rt_float const lnT1 = LOG(Temperature);
  rt_float const lnT2 = lnT1 * lnT1;
  rt_float const lnT3 = lnT1 * lnT2;
  rt_float const lnT4 = lnT1 * lnT3;
  rt_float const lnT5 = lnT1 * lnT4;
  rt_float const PreFactor = Constants->Ma * 1e-33;
  if (Temperature <= 1.0e5) {
    L_local +=
        PreFactor *
        EXP(CECoef_lo[0] + CECoef_lo[1] * lnT1 + CECoef_lo[2] * lnT2 +
            CECoef_lo[3] * lnT3 + CECoef_lo[4] * lnT4 + CECoef_lo[5] * lnT5 -
            1.184156e5 / Temperature) *
        (n_e * n_H1);
  } else {
    L_local +=
        PreFactor *
        EXP(CECoef_hi[0] + CECoef_hi[1] * lnT1 + CECoef_hi[2] * lnT2 +
            CECoef_hi[3] * lnT3 + CECoef_hi[4] * lnT4 + CECoef_hi[5] * lnT5 -
            1.184156e5 / Temperature) *
        (n_e * n_H1);
  }
#  endif /* #ifndef COOLING_COLLISIONAL_EXCITATION_OF_HYDROGEN__SPITZER #else \
          */

  /* L_C: Cooling from Inverse Compton scattering off CMB photons.
   *  10^[-29 -16 -23 - ( -31 + 8 ) + [0 to 4] + 4*0 + [4 to 8]]
   *   = 10^[-41 to -33]
   *  Susceptible to underflow.  Force double and scale by Ma.
   */
  double const Compton_coefficient =
      ((double)Constants->Ma * 4.0 * (double)Constants->sigma_T *
       (double)Constants->a * (double)Constants->k_B) /
      ((double)Constants->mass_e * (double)Constants->c);
  double const T_CMB = (double)Constants->T_CMB_0 / (double)ExpansionFactor;

  L_local +=
      (float)(Compton_coefficient * n_e * pow(T_CMB, 4.0) *
              ((double)Temperature - T_CMB));


  /*Free-free and collisional He+ rates taken from Katz, Weinberg and Hernquist (1996)*/
  /* free-free cooling */
  rt_float gaunt_factor_exponent = 5.5 - LOG10(Temperature);
  gaunt_factor_exponent *= gaunt_factor_exponent / 3.0f;
  rt_float gaunt_factor = 1.1 + 0.34 * exp(-gaunt_factor_exponent);
  //fold factor if Ma in here
  rt_float L_free = (1.42e-27*Constants->Ma) * gaunt_factor * SQRT(Temperature) * (n_H2 + n_He2 + 4*n_He3) * n_e;
  //convert cgs to mks
  L_local += L_free * 1e-7 * 1e-6;

  /* Collisional cooling He+ */
  rt_float L_cHe2 = 5.54e-17 * pow(Temperature, -0.397) * exp(-473638.0/Temperature) * n_e * n_He2;
  L_cHe2 /= 1+ sqrt(Temperature/1e5);
  //Convert cgs to mks and add factor of Ma
  L_local += L_cHe2 * (1e-7 * 1e-6 * Constants->Ma);

  Rates_dev->L[cellindex] += L_local;
#endif /* ifndef NO_COOLING */
}
