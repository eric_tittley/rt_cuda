#include "RT_CUDA.h"
#include "cudasupport.h"

__global__ void RC_CR_kernel(rt_float const ExpansionFactor,
                             RT_Data const Data_dev,
                             RT_ConstantsT const Constants,
                             /*@out@*/ RT_RatesBlockT Rates_dev) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    RT_CUDA_CoolingRatesExtra(&Constants,
                              cellindex,
                              ExpansionFactor,
                              &Data_dev,
                              /*@out@*/ &Rates_dev);
  }
}

void RT_CUDA_CoolingRates(rt_float const ExpansionFactor,
                          RT_ConstantsT const Constants,
                          RT_Data const Data_dev_RT,
                          /*@out@*/ RT_RatesBlockT const Rates_dev) {
  int blockSize;  // The launch configurator returned block size
  int gridSize;   // The actual grid size needed, based on input size
  setGridAndBlockSize(
      Data_dev_RT.NumCells, (void*)RC_CR_kernel, &gridSize, &blockSize);

  RC_CR_kernel<<<gridSize, blockSize>>>(ExpansionFactor,
                                        Data_dev_RT,
                                        Constants,
                                        /*@out@*/ Rates_dev);
}
