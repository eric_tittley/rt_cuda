#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_CUDA.h"
#include "cudasupport.h"

void RT_CUDA_CopyRatesDeviceToHost(RT_RatesBlockT const Rates_dev,
                                   RT_RatesBlockT const Rates_host) {
  /* Data and Rates must store pointers to Device Memory ! */

  size_t const NumBytes = sizeof(rt_float) * Rates_dev.NumCells;

  cudaMemcpyAsync((void *)Rates_host.I_H1,
                  (void *)Rates_dev.I_H1,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.I_He1,
                  (void *)Rates_dev.I_He1,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.I_He2,
                  (void *)Rates_dev.I_He2,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.G,
                  (void *)Rates_dev.G,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.G_H1,
                  (void *)Rates_dev.G_H1,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.G_He1,
                  (void *)Rates_dev.G_He1,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.G_He2,
                  (void *)Rates_dev.G_He2,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.alpha_H1,
                  (void *)Rates_dev.alpha_H1,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.alpha_He1,
                  (void *)Rates_dev.alpha_He1,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.alpha_He2,
                  (void *)Rates_dev.alpha_He2,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.L,
                  (void *)Rates_dev.L,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.Gamma_HI,
                  (void *)Rates_dev.Gamma_HI,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.n_HI_Equilibrium,
                  (void *)Rates_dev.n_HI_Equilibrium,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.n_HII_Equilibrium,
                  (void *)Rates_dev.n_HII_Equilibrium,
                  NumBytes,
                  cudaMemcpyDeviceToHost);
  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
  cudaMemcpyAsync((void *)Rates_host.TimeScale,
                  (void *)Rates_dev.TimeScale,
                  NumBytes,
                  cudaMemcpyDeviceToHost);

  checkCUDAErrors(
      "ERROR: Unable to copy Rates from device to host.", __FILE__, __LINE__);
}
