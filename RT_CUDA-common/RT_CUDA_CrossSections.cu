/* H1_H2, He1_He2, He2_He3	Photoionisation cross-sections.
 *
 * rt_float H1_H2(rt_float nu)	HI   -> HII
 * rt_float He1_He2(rt_float nu)	HeI  -> HeII
 * rt_float He2_He3(rt_float nu)	HeII -> HeIII
 *
 * ARGUMENTS
 *  Input, not modified
 *   nu	The frequency at which to evaluate the cross section.
 *
 * RETURNS
 *  Photoionisation cross section (units depend on how sigma_* is defined)
 *
 * EXTERNAL MACROS AND VARIABLES
 *  Cross-sections at the ionisation thresholds
 *   sigma_0   HI   -> HII
 *   sigma_1   HeI  -> HeII
 *   sigma_2   HeII -> HeIII
 *
 * NOTES
 *  - It is up to the calling routine to ensure that nu >= nu_[012]
 *    for sigma_[012] respectively.  If nu < nu_[012], then they are 0.
 *  - Interpolation formulae for cross sections from Osterbrock p.34
 *
 * CALLS
 *
 * AUTHOR: Eric Tittley
 */

#include "RT_CUDA.h"

/* C does not allow global const variables to be declared using
 * other global const variables. Pity.
 * From Osterbrock p. 34 */
#define beta_0 1.34
#define beta_1 1.66
#define beta_2 1.34
#define s_0 2.99
#define s_1 2.05
#define s_2 2.99

rt_float const s0_p_1 = (s_0 + 1.0);
rt_float const s1_p_1 = (s_1 + 1.0);
rt_float const s2_p_1 = (s_2 + 1.0);
rt_float const one_m_beta_0 = (1.0 - beta_0);
rt_float const one_m_beta_1 = (1.0 - beta_1);
rt_float const one_m_beta_2 = (1.0 - beta_2);

__device__ rt_float RT_CUDA_H1_H2(rt_float const nu,
                                  RT_ConstantsT const* const Constants) {
  rt_float t1 = beta_0 * POW((Constants->nu_0 / nu), s_0);
  rt_float t2 = one_m_beta_0 * POW((Constants->nu_0 / nu), s0_p_1);
  return Constants->sigma_0 * (t1 + t2);
}

__device__ rt_float RT_CUDA_He1_He2(rt_float const nu,
                                    RT_ConstantsT const* const Constants) {
  rt_float t1 = beta_1 * POW((Constants->nu_1 / nu), s_1);
  rt_float t2 = one_m_beta_1 * POW((Constants->nu_1 / nu), s1_p_1);
  return Constants->sigma_1 * (t1 + t2);
}

__device__ rt_float RT_CUDA_He2_He3(rt_float const nu,
                                    RT_ConstantsT const* const Constants) {
  rt_float t1 = beta_2 * POW((Constants->nu_2 / nu), s_2);
  rt_float t2 = one_m_beta_2 * POW((Constants->nu_2 / nu), s2_p_1);
  return Constants->sigma_2 * (t1 + t2);
}
