#pragma once
#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <cuda_runtime.h>

#include "RT_CUDA.h"

inline __device__ void RT_CUDA_RecombinationRatesInclCooling(
    size_t const cellindex,
    rt_float const ExpansionFactor,
    RT_ConstantsT const* const Constants,
    RT_Data const* const Data_dev,
    /*@out@*/ RT_RatesBlockT* const Rates_dev) {
  rt_float const n_H1 = Data_dev->n_H[cellindex] * Data_dev->f_H1[cellindex];
  rt_float const n_H2 = Data_dev->n_H[cellindex] * Data_dev->f_H2[cellindex];
  rt_float const n_He2 = Data_dev->n_He[cellindex] * Data_dev->f_He2[cellindex];
  rt_float const n_He3 = Data_dev->n_He[cellindex] * Data_dev->f_He3[cellindex];

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);

  rt_float const Temperature = RT_CUDA_TemperatureFromEntropy(
      Data_dev->Entropy[cellindex], cellindex, Data_dev, Constants);
  rt_float const tau_H1_cell =
      Constants->sigma_0 * n_H1 * Data_dev->dR[cellindex];

  /* The Recombination Coefficients */
  rt_float beta_H1, beta_He1, beta_He2;
  RT_CUDA_RecombinationCoefficients(tau_H1_cell,
                                    Temperature,
                                    &(Rates_dev->alpha_H1[cellindex]),
                                    &(Rates_dev->alpha_He1[cellindex]),
                                    &(Rates_dev->alpha_He2[cellindex]),
                                    &beta_H1,
                                    &beta_He1,
                                    &beta_He2);

#ifndef DISABLE_RECOMBINAION_COOLING
  Rates_dev->L[cellindex] += n_e * n_H2 * (beta_H1 * Constants->Ma);
  Rates_dev->L[cellindex] += n_e * n_He2 * (beta_He1 * Constants->Ma);
  Rates_dev->L[cellindex] += n_e * n_He3 * (beta_He2 * Constants->Ma);
#endif
}
