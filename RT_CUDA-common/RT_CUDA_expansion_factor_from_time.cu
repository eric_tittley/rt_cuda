/* expansion_factor_from_time: The expansion factor from the age of the
 *	universe.
 *
 * rt_float expansion_factor_from_time(rt_float t)
 *
 * ARGUMENTS
 *  Input, not modified
 *   t	The age of the universe. [s]
 *
 * RETURNS
 *   The expansion factor.
 *
 * AUTHOR: Eric Tittley
 */

/* #include <math.h> */

#include "RT_CUDA.h"

__device__ rt_float
RT_CUDA_expansion_factor_from_time(rt_float const t,
                                   RT_ConstantsT const* const Constants) {
  rt_float const H_0 = Constants->h * 1e5 / Constants->Mpc; /* s^{-1} */

  rt_float a_local, a_term_1, a_term_2;
  if ((Constants->omega_m > Constants->OneMinusEps) &&
      (Constants->omega_m < Constants->OnePlusEps)) {
    a_local = POW(1.5 * t * H_0, 2. / 3.);
  } else {
    /* a_term_1 = POW(omega_m/(1.-omega_m),1./3.); */
    a_term_1 = CBRT(Constants->omega_m / (1. - Constants->omega_m));
    a_term_2 =
        POW(SINH(3. / 2. * H_0 * SQRT(1. - Constants->omega_m) * t), 2. / 3.);
    a_local = a_term_1 * a_term_2;
  }
  return a_local;
}
