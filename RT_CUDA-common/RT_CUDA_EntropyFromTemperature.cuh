#pragma once
#include <cuda_runtime.h>

#include "RT_CUDA.h"

inline __device__ rt_float
RT_CUDA_EntropyFromTemperature(rt_float const Temperature,
                               size_t const iCell,
                               RT_Data const* const Data,
                               RT_ConstantsT const* const Constants) {
  /* Update the neutral & ionised number densities. */
  rt_float const n_H2 = Data->n_H[iCell] * Data->f_H2[iCell];
  rt_float const n_He2 = Data->n_He[iCell] * Data->f_He2[iCell];
  rt_float const n_He3 = Data->n_He[iCell] * Data->f_He3[iCell];

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2. * n_He3);

  /* Total number density */
  rt_float const n = Data->n_H[iCell] + Data->n_He[iCell] + n_e;

  rt_float const mu = (Data->n_H[iCell] + 4. * Data->n_He[iCell]) / n;

  /* Numeric bounds check:
   *  10^( (-23 + 4 - -27) - ( 0 + 2/3 * (-28) )
   *  10^( +8  - ( -18 ) )
   *  10^26
   */
  rt_float const Entropy =
      (Constants->k_B * Temperature / Constants->mass_p) /
      (mu * POW(Data->Density[iCell], Constants->gamma_ad - 1.));

  return Entropy;
}
