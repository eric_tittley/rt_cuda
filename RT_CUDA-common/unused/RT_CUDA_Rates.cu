/* This function is deprecated as 1) It requires too many registers. 
 *                              & 2) No suitable ExtraCooling function exists. */


/* PREPROCESSOR MACROS
 *  PLANE_WAVE
 *  CONSTANT_DENSITY
 *  CONSTANT_DENSITY_COMOVING
 */

#include "RT_CUDA.h"

#ifdef DEBUG
# include <nppdefs.h>
#endif

#ifndef M_PI
# define M_PI           3.14159265358979323846  /* pi */
#endif

__device__ void
RT_CUDA_Rates(size_t const cellindex,
              rt_float const ExpansionFactor,
              rt_float const tCurrent,
              RT_SourceT const * const Source,
              RT_ConstantsT const * const Constants,
              RT_Data const * const Data_dev,
    /*@out@*/ RT_RatesT * const Rates ) {

printf("WARNING: Calling deprecated function: %s\n",__FILE__);

 /* Find the neutral & ionised number densities. */
 rt_float const n_H1  = Data_dev->n_H[cellindex]  * Data_dev->f_H1[cellindex];
 rt_float const n_He1 = Data_dev->n_He[cellindex] * Data_dev->f_He1[cellindex];
 rt_float const n_He2 = Data_dev->n_He[cellindex] * Data_dev->f_He2[cellindex];

 /* Column density through the shell */
 rt_float const N_H1  = n_H1  * Data_dev->dR[cellindex];
 rt_float const N_He1 = n_He1 * Data_dev->dR[cellindex];
 rt_float const N_He2 = n_He2 * Data_dev->dR[cellindex];

#ifdef DEBUG
 /* For debugging */
 if( N_H1<-NPP_MINABS_32F || !isfinite(N_H1) ) {
  printf("%s: %i: ERROR: N_H1=%5.3e; n_H1=%5.3e; dR[%3lu]=%5.3e; n_H=%5.3e; f_H1=%5.3e\n",
         __FILE__,__LINE__,N_H1,n_H1,cellindex,Data_dev->dR[cellindex],Data_dev->n_H[cellindex],Data_dev->f_H1[cellindex]);
  return;
 }
 if( N_He1<-NPP_MINABS_32F || !isfinite(N_He1) ) {
  printf("%s: %i: ERROR: N_He1=%e\n",__FILE__,__LINE__,N_He1);
  return;
 }
 if( N_He2<-NPP_MINABS_32F|| !isfinite(N_He2) ) {
  printf("%s: %i: ERROR: N_He2=%e\n",__FILE__,__LINE__,N_He2);
  return;
 }
#endif

 /* Find the redshift of the *source*, in the frame of reference of the
  * observer.  Simply to correct the source spectrum for the light-travel
  * time. This is only correct over small R. */
 rt_float const a_source = RT_CUDA_expansion_factor_from_time(
                              tCurrent-Data_dev->R[cellindex]/Constants->c,
                              Constants);

 rt_float const DistanceFromSourceMpc = Data_dev->R[cellindex]/Constants->Mpc;
 rt_float const ParticleRadiusMpc = Data_dev->dR[cellindex]/Constants->Mpc;
 rt_float const DistanceThroughElementMpc = (4./3.)*ParticleRadiusMpc;

 /* ******* Start of CPU-intensive code ******* */
 /* ****** 100% of the time is spent here ***** */
 RT_CUDA_PhotoionizationRates(N_H1,
                              N_He1,
                              N_He2,
                              Data_dev->column_H1[cellindex],
                              Data_dev->column_He1[cellindex],
                              Data_dev->column_He2[cellindex],
                              (1./a_source-1.),
                              Constants,
                              Source,
                              DistanceFromSourceMpc,
                              ParticleRadiusMpc,
                              DistanceThroughElementMpc,
                              &(Rates->I_H1),
                              &(Rates->I_He1),
                              &(Rates->I_He2),
                              &(Rates->G_H1),
                              &(Rates->G_He1),
                              &(Rates->G_He2),
                              &(Rates->Gamma_HI));
 /* ******* End of CPU-intensive code ******* */

#ifdef DEBUG
 /* For debugging */
 if(Rates->I_H1<0. || !isfinite(Rates->I_H1) ) {
  printf("%s: %i: ERROR: Rates->I_H1=%e\n",__FILE__,__LINE__,Rates->I_H1);
  return;
 }
 if(Rates->I_He1<0. || !isfinite(Rates->I_He1) ) {
  printf("%s: %i: ERROR: Rates->I_He1=%e\n",__FILE__,__LINE__,Rates->I_He1);
  return;
 }
 if(Rates->I_He2<0. || !isfinite(Rates->I_He2) ) {
  printf("%s: %i: ERROR: Rates->I_He2=%e\n",__FILE__,__LINE__,Rates->I_He2);
  return;
 }
 if(Rates->G_H1<0. || !isfinite(Rates->G_H1) )  {
  printf("%s: %i: ERROR: Rates->G_H1=%e\n",__FILE__,__LINE__,Rates->G_H1);
  return;
 }
 if(Rates->G_He1<0. || !isfinite(Rates->G_He1) ) {
  printf("%s: %i: ERROR: Rates->G_He1=%e\n",__FILE__,__LINE__,Rates->G_He1);
  return;
 }
 if(Rates->G_He2<0. || !isfinite(Rates->G_He2) ) {
  printf("%s: %i: ERROR: Rates->G_He2=%e\n",__FILE__,__LINE__,Rates->G_He2);
  return;
 }
#endif

 /* Total heating rate J m^-3 s^-1 */
 Rates->G = Rates->G_H1 + Rates->G_He1 + Rates->G_He2;

 rt_float const n_H2  = Data_dev->n_H[cellindex]  * Data_dev->f_H2[cellindex];
 rt_float const n_He3 = Data_dev->n_He[cellindex] * Data_dev->f_He3[cellindex];

 /* Electron density */
 rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);

#ifndef CONSTANT_TEMPERATURE
 rt_float const
 Temperature = RT_CUDA_TemperatureFromEntropy(Data_dev->Entropy[cellindex],
                                              cellindex,
                                              Data_dev,
                                              Constants );
 rt_float const tau_H1_cell = Constants->sigma_0 * n_H1 * Data_dev->dR[cellindex] ;
#else
 rt_float const Temperature = CONSTANT_TEMPERATURE;
#endif

 /* The Recombination Coefficients */
 rt_float beta_H1, beta_He1, beta_He2;
 RT_CUDA_RecombinationCoefficients(tau_H1_cell,
                                   Temperature,
                                  &(Rates->alpha_H1),
                                  &(Rates->alpha_He1),
                                  &(Rates->alpha_He2),
                                  &beta_H1,
                                  &beta_He1,
                                  &beta_He2);

 Rates->L += n_e * n_H2  * (beta_H1  * Constants->Ma);
 Rates->L += n_e * n_He2 * (beta_He1 * Constants->Ma);
 Rates->L += n_e * n_He3 * (beta_He2 * Constants->Ma);
}
