NOT FINISHED!!!! See Taranis/libTaranis/calculateSppRates_V.cu
for an example of how to do it, but for when 
number of source-particle-pairs != number of particles.


__inline__ __device__
float warpReduceSum(float val) {
 int warpSize=16;
 for (int offset = warpSize/2; offset > 0; offset /= 2) 
  val += __shfl_down(val, offset);
 return val;
}

/**********************************************************************
 * Integration via parts: nu_0 to nu_1, nu_1 to nu_2, nu_2 to nu_end. *
 * This assists convergence since it avoid the discontinuities at     *
 * these frequencies.                                                 *
 **********************************************************************/
__device__ void
RTC_PR_V_Integrate(rt_float const N_H1,
                   rt_float const N_He1,
                   rt_float const N_He2,
                   rt_float const N_H1_cum,
                   rt_float const N_He1_cum,
                   rt_float const N_He2_cum,
                   rt_float const z,
                   RT_ConstantsT const * const Constants,
                   RT_SourceT const * const Source,
         /*@out@*/ rt_float * const f)
{
 rt_float sum[7];

 for(size_t i=0;i<7;i++) {
  f[i]=0.;
 }
 RT_CUDA_Gauss_Legendre(Constants->nu_0,
                        Constants->nu_1,
                        N_H1,
                        N_He1,
                        N_He2,
                        N_H1_cum,
                        N_He1_cum,
                        N_He2_cum,
                        z,
                        Constants,
                        Source,
                        0,
                        sum);
#ifdef DEBUG
 for(size_t i=0;i<7;i++) {
  if( (!isfinite(sum[i])) || (sum[i]<-NPP_MINABS_32F) ){
   printf("%s: %i: ERROR: sum[%lu]=%e\n",__FILE__,__LINE__,i,sum[i]);
   return;
  }
 }
#endif
 for(size_t i=0;i<7;i++) {
  f[i] += sum[i];
 }
#ifdef DEBUG
 for(size_t i=0;i<7;i++) {
  if((!isfinite(f[i])) || (f[i]<-NPP_MINABS_32F)){
   printf("%s: %i: ERROR: f[%lu]=%e\n",__FILE__,__LINE__,i,f[i]);
   return;
  }
 }
#endif

 RT_CUDA_Gauss_Legendre(Constants->nu_1,
                        Constants->nu_2,
                        N_H1,
                        N_He1,
                        N_He2,
                        N_H1_cum,
                        N_He1_cum,
                        N_He2_cum,
                        z,
                        Constants,
                        Source,
                        1,
                        sum);
#ifdef DEBUG
 for(size_t i=0;i<7;i++) {
  if( (!isfinite(sum[i])) || (sum[i]<-NPP_MINABS_32F) ){
   printf("%s: %i: ERROR: sum[%lu]=%e\n",__FILE__,__LINE__,i,sum[i]);
   return;
  }
 }
#endif
 for(size_t i=0;i<7;i++) {
  f[i] += sum[i];
 }
#ifdef DEBUG
 for(size_t i=0;i<7;i++) {
  if((!isfinite(f[i])) || (f[i]<-NPP_MINABS_32F)){
   printf("%s: %i: ERROR: f[%lu]=%e\n",__FILE__,__LINE__,i,f[i]);
   return;
  }
 }
#endif

 RT_CUDA_Gauss_Laguerre(Constants->nu_2,
                        Source->nu_end,
                        N_H1,
                        N_He1,
                        N_He2,
                        N_H1_cum,
                        N_He1_cum,
                        N_He2_cum,
                        z,
                        Constants,
                        Source,
                        2,
                        sum);
#ifdef DEBUG
 for(size_t i=0;i<7;i++) {
  if( (!isfinite(sum[i])) || (sum[i]<-NPP_MINABS_32F) ){
   printf("%s: %i: ERROR: sum[%lu]=%e\n",__FILE__,__LINE__,i,sum[i]);
   return;
  }
 }
#endif
 for(size_t i=0;i<7;i++) {
  f[i] += sum[i];
 }
#ifdef DEBUG
 for(size_t i=0;i<7;i++) {
  if((!isfinite(f[i])) || (f[i]<-NPP_MINABS_32F)){
   printf("%s: %i: ERROR: f[%lu]=%e; sum=%5.3e;\n",__FILE__,__LINE__,i,f[i],sum[i]);
   return;
  }
 }
#endif

}



__device__ void
RTC_PR(rt_float const N_H1,
       rt_float const N_He1,
       rt_float const N_He2,
       rt_float const N_H1_cum,
       rt_float const N_He1_cum,
       rt_float const N_He2_cum,
       rt_float const RedshiftOfSource,
       RT_ConstantsT const * const Constants,
       RT_SourceT const * const Source,
       rt_float const DistanceFromSourceMpc,
       rt_float const ParticleRadiusMpc,
       rt_float const DistanceThroughElementMpc,
       rt_float * const I_H1,
       rt_float * const I_He1,
       rt_float * const I_He2,
       rt_float * const G_H1,
       rt_float * const G_He1,
       rt_float * const G_He2,
       rt_float * const Gamma_HI)
{
 rt_float f[7];
 if(Source->SourceType!=SourceMonochromatic) {
  RT_CUDA_PhotoionizationRatesIntegrate_V(N_H1,
                                          N_He1,
                                          N_He2,
                                          N_H1_cum,
                                          N_He1_cum,
                                          N_He2_cum,
                                          RedshiftOfSource,
                                          Constants,
                                          Source,
                                          f);
 } else {
  RT_CUDA_PhotoionizationRatesMonochromatic(N_H1,
                                            N_He1,
                                            N_He2,
                                            N_H1_cum,
                                            N_He1_cum,
                                            N_He2_cum,
                                            RedshiftOfSource,
                                            Source,
                                            Constants,
                                            f);
 }

);



/* Kernel */
__global__ void
RTC_PR_V( rt_float const nu,
          rt_float const W,
	  rt_float const L,
	  rt_float const Alpha,
          RT_Data const * const Data,
          rt_float const * const 
/*@out@*/ RT_RatesBlockT * const Rates
        ) {

 rt_float f[7];
 /* This should be in global memory until Alpha is refactored */
 size_t const IntervalIndex[]  = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2};
 size_t const FrequencyIndex[] = [0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 4, 5, 6, 7];

 size_t const istart = blockIdx.x*blockDim.x+threadIdx.x;
 size_t const span   = gridDim.x * blockDim.x; // nBlocks * nThreadsPerBlock

 for( size_t iMarker=istart; iMarker<Data_dev.NumCells*nFrequencies; iMarker+=span ) {

  /* There's a separate thread for each frequency of every element. */
  size_t const ielement   = iMarker / nFrequencies;
  size_t const ifrequency = iMarker % nFrequencies;

  rt_float const nu = nus[ifrequency];
  rt_float const W  = Ws[ifrequency];

  /* This thread is part of a half warp, so though I'm doing a single frequency, when I return
   * from the following call, all frequencies for this RT element will have been processed and
   * the elements of f will be ready for summing */
  /* Constants.Alpha is currently Alpha[0][0:3]
   *                              Alpha[1][0:3]
   *                              Alpha[2][0:4]
   * Unless Alpha is refactored to be [0:nFrequencies-1], we will need to convert */
  rt_float const Alpha = Constants->Alpha[IntervalIndex[ifrequency]][FrequencyIndex[ifrequency]];
  RT_CUDA_PhotoionizationRatesFunctions(nu,
                                        n_H1,
                                        N_He1,
                                        N_He2,
                                        N_H1_cum,
                                        N_He1_cum,
                                        N_He2_cum,
                                        Redshift,
                                        Source,
                                        Constants,
                                        Alpha,
                                        f);

  /* Apply the weights */
  for(size_t i=0;i<7;++i) {
   f[i] *= W;
  }

  /* Sum up each of the f[]'s across the half-warp */
  for(size_t i=0;i<7;++i) {
   rt_float f_local = f[i];
   f_local warpReduceSum(f_local);
   f[i] = f_local;
  }

  /* Only the thread with ifrequency==0 has the correct f[]'s */
  if(ifrequency==0) {
   RTC_PR_Attenuate(f,
   		    Constants,
   		    Source,
   		    ExpansionFactor,
   		    Data.R[ielement]/Constants->Mpc,
   		    DistanceThroughElementMpc,
   		    Data.dR[ielement]/Constants->Mpc,
   		   &(Rates->I_H1[ ielement]),
   		   &(Rates->I_He1[ielement]),
   		   &(Rates->I_He2[ielement]),
   		   &(Rates->G_H1[ ielement]),
   		   &(Rates->G_He1[ielement]),
   		   &(Rates->G_He2[ielement]),
   		   &(Rates->Gamma_HI[ielement]);
  }
 }
}

/* Convert span [-1,1] to [a,b] */
rt_float
RT_CUDA_Gauss_Legendre_x_trans(rt_float const t, rt_float const a, rt_float const b) {
 return (a+b)/2. + t*(b-a)/2;
}
 


RT_CUDA_PhotoIonizationRates_V() {

 /* Frequencies and Weights. Calculate on CPU and copy to GPU global memory */
 rt_float *nu_d;
 rt_float *W_d;
 RTC_PR_SetFrequenciesAndWeights(&nu_d, &W_d);



 cudaFree(nu_d);
 cudaFree(W_d);
}
