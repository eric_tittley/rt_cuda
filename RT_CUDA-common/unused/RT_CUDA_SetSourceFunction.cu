#include <stdio.h>

#include "RT_CUDA.h"

__device__ void
RT_CUDA_SetSourceFunction(RT_SourceT * const Source) {
 switch(Source->SourceType) {
  case SourceBlackBody:                Source->LuminosityFunction=(SourceFunctionT)RT_CUDA_Luminosity_BLACK_BODY;                   break;
  case SourceHybrid:                   Source->LuminosityFunction=(SourceFunctionT)RT_CUDA_Luminosity_HYBRID;                       break;
  case SourcePowerLawMinusOneHalf:     Source->LuminosityFunction=(SourceFunctionT)RT_CUDA_Luminosity_POWER_LAW_MINUS_ONE_HALF;     break;
  case SourcePowerLawMinusOne:         Source->LuminosityFunction=(SourceFunctionT)RT_CUDA_Luminosity_POWER_LAW_MINUS_ONE;          break;
  case SourcePowerLawMinusThreeHalves: Source->LuminosityFunction=(SourceFunctionT)RT_CUDA_Luminosity_POWER_LAW_MINUS_THREE_HALVES; break;
  case SourcePowerLawMinusTwo:         Source->LuminosityFunction=(SourceFunctionT)RT_CUDA_Luminosity_POWER_LAW_MINUS_TWO;          break;
  case SourcePowerLaw:                 Source->LuminosityFunction=(SourceFunctionT)RT_CUDA_Luminosity_POWER_LAW;                    break;
  case SourceMiniQuasar:               Source->LuminosityFunction=(SourceFunctionT)RT_CUDA_Luminosity_SOURCE_MINI_QUASAR;           break;
  case SourceMonochromatic:            Source->LuminosityFunction=(SourceFunctionT)RT_CUDA_Luminosity_MONOCHROMATIC;                break;
  case SourceTabulated:                Source->LuminosityFunction=(SourceFunctionT)RT_CUDA_Luminosity_TABULATED;                    break;
  default:
   printf("ERROR: %s: Unknown Source Type or Source Type not set: %i\n",
          __FILE__,Source->SourceType);
 }
}
