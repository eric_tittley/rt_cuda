/* PREPROCESSOR MACROS
 *  ADAPTIVE_TIMESCALE (default on)
 */

#include <stdlib.h>

#include "RT_CUDA.h"

__device__ void
RT_CUDA_ProcessCell(size_t const losid,
                    size_t const iz,
                    size_t const cellindex,
                    bool const LastIterationFlag,
		    rt_float const ExpansionFactor,
                    rt_float const tCurrent,
                    rt_float const dt,
                    rt_float const dt_RT,
                    RT_SourceT const * const Source,
  		    RT_ConstantsT const * const Constants,
		    RT_Data * const Data_dev,
		    rt_float * const AdapScales ) {

 RT_RatesT Rates;

 /* Calculate all rates in the cell */
 RT_CUDA_Rates(cellindex,
               ExpansionFactor,
               tCurrent,
               Source,
               Constants,
               Data_dev,
              &Rates);

#ifdef ADAPTIVE_TIMESCALE
 /* Find the minimum timescale for this cell (stored in AdapScales) */
 RT_CUDA_AdaptiveTimescale(losid,
                           iz,
                           cellindex,
                           dt,
                           Constants,
     			  &Rates,
                           Data_dev,
                           AdapScales);
#endif /* ADAPTIVE_TIMESCALE */

 /* Update Temperature & Ionization states for the cell for this timestep. */
 RT_CUDA_UpdateCell(dt_RT,
                    ExpansionFactor,
                    cellindex,
                    Constants,
                   &Rates,
                    Data_dev);

 if(LastIterationFlag) { /* Only during last sub-timestep */
  RT_CUDA_LastIterationUpdate(cellindex, Constants, &Rates, Data_dev);
 }

}
