/* PREPROCESSOR MACROS
 *  PLANE_WAVE
 *  CONSTANT_DENSITY
 *  CONSTANT_DENSITY_COMOVING
 */

#ifndef M_PI
# define M_PI           3.14159265358979323846  /* pi */
#endif

#include "RT_CUDA.h"

__device__ void
RT_CUDA_Rates_RatesBlock(size_t const cellindex,
              rt_float const ExpansionFactor,
              rt_float const tCurrent,
              RT_SourceT const * const Source,
              RT_ConstantsT const * const Constants,
              RT_Data const * const Data_dev,
    /*@out@*/ RT_RatesBlockT * const Rates_dev ) {

 /* Find the neutral & ionised number densities. */
 rt_float const n_H1  = Data_dev->n_H[cellindex]  * Data_dev->f_H1[cellindex];
 rt_float const n_He1 = Data_dev->n_He[cellindex] * Data_dev->f_He1[cellindex];
 rt_float const n_He2 = Data_dev->n_He[cellindex] * Data_dev->f_He2[cellindex];

 /* Column density through the shell */
 rt_float const N_H1  = n_H1  * Data_dev->dR[cellindex];
 rt_float const N_He1 = n_He1 * Data_dev->dR[cellindex];
 rt_float const N_He2 = n_He2 * Data_dev->dR[cellindex];

 /* Find the redshift of the *source*, in the frame of reference of the
  * observer.  Simply to correct the source spectrum for the light-travel
  * time. This is only correct over small R. */
 rt_float const a_source = RT_CUDA_expansion_factor_from_time(
                              tCurrent-Data_dev->R[cellindex]/Constants->c,
                              Constants);

 rt_float const DistanceFromSourceMpc = Data_dev->R[cellindex]/Constants->Mpc;
 rt_float const ParticleRadiusMpc = Data_dev->dR[cellindex]/Constants->Mpc;
 rt_float const DistanceThroughElementMpc = (4./3.)*ParticleRadiusMpc;

 /* ******* Start of CPU-intensive code ******* */
 /* ****** 100% of the time is spent here ***** */
 RT_CUDA_PhotoionizationRates(N_H1,
                              N_He1,
                              N_He2,
                              Data_dev->column_H1[cellindex],
                              Data_dev->column_He1[cellindex],
                              Data_dev->column_He2[cellindex],
                              (1./a_source-1.),
                              Constants,
                              Source,
                              DistanceFromSourceMpc,
			      ParticleRadiusMpc,
			      DistanceThroughElementMpc,
                             &(Rates_dev->I_H1[cellindex]),
                             &(Rates_dev->I_He1[cellindex]),
                             &(Rates_dev->I_He2[cellindex]),
                             &(Rates_dev->G_H1[cellindex]),
                             &(Rates_dev->G_He1[cellindex]),
                             &(Rates_dev->G_He2[cellindex]),
                             &(Rates_dev->Gamma_HI[cellindex]));
 /* ******* End of CPU-intensive code ******* */

#ifdef DEBUG
 /* For debugging */
 if(Rates_dev->I_H1[cellindex]<0. || !isfinite(Rates_dev->I_H1[cellindex]) ) {
  printf("%s: %i: ERROR: Rates_dev->I_H1=%e\n",__FILE__,__LINE__,Rates_dev->I_H1[cellindex]);
  return;
 }
 if(Rates_dev->I_He1[cellindex]<0. || !isfinite(Rates_dev->I_He1[cellindex]) ) {
  printf("%s: %i: ERROR: Rates_dev->I_He1=%e\n",__FILE__,__LINE__,Rates_dev->I_He1[cellindex]);
  return;
 }
 if(Rates_dev->I_He2[cellindex]<0. || !isfinite(Rates_dev->I_He2[cellindex]) ) {
  printf("%s: %i: ERROR: Rates_dev->I_He2=%e\n",__FILE__,__LINE__,Rates_dev->I_He2[cellindex]);
  return;
 }
 if(Rates_dev->G_H1[cellindex]<0. || !isfinite(Rates_dev->G_H1[cellindex]) )  {
  printf("%s: %i: ERROR: Rates_dev->G_H1=%e\n",__FILE__,__LINE__,Rates_dev->G_H1[cellindex]);
  return;
 }
 if(Rates_dev->G_He1[cellindex]<0. || !isfinite(Rates_dev->G_He1[cellindex]) ) {
  printf("%s: %i: ERROR: Rates_dev->G_He1=%e\n",__FILE__,__LINE__,Rates_dev->G_He1[cellindex]);
  return;
 }
 if(Rates_dev->G_He2[cellindex]<0. || !isfinite(Rates_dev->G_He2[cellindex]) ) {
  printf("%s: %i: ERROR: Rates_dev->G_He2=%e\n",__FILE__,__LINE__,Rates_dev->G_He2[cellindex]);
  return;
 }
#endif

 /* Total heating rate J m^-3 s^-1 */
 Rates_dev->G[cellindex] =  Rates_dev->G_H1[cellindex]
                          + Rates_dev->G_He1[cellindex]
                          + Rates_dev->G_He2[cellindex];

 Rates_dev->L[cellindex] = 0.0;
 RT_CUDA_RecombinationRatesInclCooling(cellindex,
                                       ExpansionFactor,
                                       Constants,
                                       Data_dev,
                             /*@out@*/ Rates_dev);

#ifndef NO_COOLING
 RT_CUDA_CoolingRatesExtra(Constants,
                           cellindex,
                           ExpansionFactor,
                           Data_dev,
                 /*@out@*/ Rates_dev
                           );
#endif
}
