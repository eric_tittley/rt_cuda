/* The source luminosity function, in W / Hz
 *
 * ARGUMENTS
 *  nu	The frequency (Hz)
 *  z	The redshift of the source, as seen from the position being evaluated.
 *  Source Parameters particular to the source
 *
 * RETURNS
 *  The source luminosity, in W/Hz
 *
 * AUTHOR: Eric Tittley
 */

/* This is a very frequently called function, accounting for up to %80 of the
 * CPU time.  Hence it is *much* better to use a fixed function for the source
 * spectrum, and use intrinsic functions instead of POW() whenever possible.
 *
 * We will set a fixed function by assigning function pointer.
 * i.e., somewhere in the initialization:
 *  rt_float (*RT_Luminosity)(rt_float, rt_float, *RT_SourceT);
 *  RT_Luminosity = &RT_Luminosity_BLACK_BODY; */

#include "RT_CUDA.h"
#include "Epsilon.h"

#include "RT_CUDA_Precision.h"

__device__ rt_float
RT_CUDA_Luminosity_BLACK_BODY( RT_LUMINOSITY_ARGS )
{
 /* Blackbody */
 /* h_p, k_B Physical Constants defined globally */
 /* Needs from Source:
  *  T_BB Temperature of the source (K)
  *  L_0 Surface area of the source (m^2) */
 if( (z > Source->z_on) || (z < Source->z_off) ) return 0.;
 rt_float exponent = Constants->h_p*nu/(Constants->k_B*Source->T_BB);
 if( exponent > LN_MAX_FLT ) return 0.;
 //return Source->SurfaceArea * (2.0*Constants->h_p/(Constants->c*Constants->c))*nu*nu*nu / ( EXP(exponent) - 1.0 );
 /* 10^(19 + ( -34 -(2*8) ) + 3*15 ) 
  * Rearrange as:
  * 10^(19 + (15-34) + (15-8) + (15-8))*/
 return   (Source->SurfaceArea * (2.0*Constants->h_p*nu))
        * M_PI
        * (nu/Constants->c)
        * (nu/Constants->c)
        / ( EXP(exponent) - 1.0 );
}

__device__ rt_float
RT_CUDA_Luminosity_HYBRID( RT_LUMINOSITY_ARGS ) {
 /* Starts as a Source1 and ends with Source2, with a smooth transition.
  * This function is removable, since we could just generate two sources,
  * one after the other. */
 rt_float fraction_of_z_trans, Lum1, Lum2;
 /* Re-cast the LuminosityFunction from void (*)(void *) */
 rt_float (*LuminosityFunction1)( RT_LUMINOSITY_ARGS ) = (rt_float (*)( RT_LUMINOSITY_ARGS ))Source->Source1->LuminosityFunction; 
 rt_float (*LuminosityFunction2)( RT_LUMINOSITY_ARGS ) = (rt_float (*)( RT_LUMINOSITY_ARGS ))Source->Source2->LuminosityFunction; 
 if( (z > Source->z_on) || (z < Source->z_off) ) return 0.;
 if( z > Source->z_trans_start ) {
  /* First phase */
  return (*(LuminosityFunction1))(nu,z,Source->Source1,Constants);
 } else if( z < Source->z_trans_finish ) {
  /* Second phase */
  return (*(LuminosityFunction2))(nu,z,Source->Source2,Constants);
 } else {
  /* Transition phase */
  /* Remember, z_trans_start > z_trans_finish */
  fraction_of_z_trans = (z-Source->z_trans_finish)
                       /(Source->z_trans_start - Source->z_trans_finish);
  Lum1 = (*(LuminosityFunction1))(nu,z,Source->Source1,Constants);
  Lum2 = (*(LuminosityFunction2))(nu,z,Source->Source2,Constants);
  return fraction_of_z_trans * Lum1 + (1.0 - fraction_of_z_trans) * Lum2;
 }
}

__device__ rt_float
RT_CUDA_Luminosity_POWER_LAW_MINUS_ONE_HALF( RT_LUMINOSITY_ARGS ) {
 if( (z > Source->z_on) || (z < Source->z_off) ) return 0.;
 return Source->L_0 * SQRT(Constants->nu_0/nu); /* alpha = -0.5 */
}

__device__ rt_float
RT_CUDA_Luminosity_POWER_LAW_MINUS_ONE( RT_LUMINOSITY_ARGS ) {
 if( (z > Source->z_on) || (z < Source->z_off) ) return 0.;
 return Source->L_0 * (Constants->nu_0/nu);     /* alpha = -1 */
}

__device__ rt_float
RT_CUDA_Luminosity_POWER_LAW_MINUS_THREE_HALVES( RT_LUMINOSITY_ARGS ) {
 if( (z > Source->z_on) || (z < Source->z_off) ) return 0.;
 return Source->L_0 * (Constants->nu_0/nu) * SQRT(Constants->nu_0/nu); /* alpha = -1.5 */
}

__device__ rt_float
RT_CUDA_Luminosity_POWER_LAW_MINUS_TWO( RT_LUMINOSITY_ARGS ) {
 if( (z > Source->z_on) || (z < Source->z_off) ) return 0.;
 return Source->L_0 * (Constants->nu_0*Constants->nu_0) / (nu*nu); /* alpha = -2 */
}

__device__ rt_float
RT_CUDA_Luminosity_POWER_LAW( RT_LUMINOSITY_ARGS ) {
 if( (z > Source->z_on) || (z < Source->z_off) ) return 0.;
 return Source->L_0 * POW(nu/Constants->nu_0,Source->alpha);
}

__device__ rt_float
RT_CUDA_Luminosity_SOURCE_MINI_QUASAR( RT_LUMINOSITY_ARGS ) {
 if( (z > Source->z_on) || (z < Source->z_off) ) return 0.;
 rt_float x=nu/Constants->nu_0;
 if( (x>=1.) && (x<20.) ) return Source->L_0*( POW(x,0.25) + 8.0/x );
 else if( (x>=20.) && (x<150.) ) return Source->L_0*8.0/x;
 else return 0.;
}

__device__ rt_float
RT_CUDA_Luminosity_MONOCHROMATIC( RT_LUMINOSITY_ARGS ) {
 if( (z > Source->z_on) || (z < Source->z_off) ) return 0.;
 return Source->L_0;
}

__device__ rt_float
RT_CUDA_Luminosity_TABULATED( RT_LUMINOSITY_ARGS ) {
 if( (z > Source->z_on) || (z < Source->z_off) ) return 0.;
 return Source->L_0 * RT_CUDA_InterpolateFromTable(nu,
                                                   Source->source_N,
                                                   Source->source_nu,
                                                   Source->source_f);
}
