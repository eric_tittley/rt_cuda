/* Return a structure in host memory with pointers to device memory */

#include <stdlib.h>

#include "RT_CUDA.h"
#include "RT_IntegralInvolatilesT.h"
#include "cudasupport.h"

#if 0
__global__ void
printFrequencies(RT_IntegralInvolatilesT Involatiles_d) {
 if(threadIdx.x==0) {
  for(size_t i=0; i<Involatiles_d.nFrequencies; ++i) {
   printf("%lu %5.3e %5.3e\n",i, Involatiles_d.Frequencies[i], Involatiles_d.Weights[i]);
   //printf("%lu %5.3e %5.3e\n",i, Frequencies[i], Weights[i] );
  }
 }
}
#endif

int RT_CUDA_copyIntegralInvolatilesToDevice(
    RT_IntegralInvolatilesT const Involatiles_h,
    RT_IntegralInvolatilesT const Involatiles_d) {
  checkCUDAErrors(
      "Got to RT_CUDA_copyIntegralInvolatilesToDevice already with errors.",
      __FILE__,
      __LINE__);

  size_t MemoryRequired = Involatiles_h.nFrequencies * sizeof(rt_float);

  /* Copy contents */

  cudaMemcpy(Involatiles_d.Frequencies,
             Involatiles_h.Frequencies,
             MemoryRequired,
             cudaMemcpyHostToDevice);
  checkCUDAErrors(
      "Unable to copy Frequencies from host to device.", __FILE__, __LINE__);

  cudaMemcpy(Involatiles_d.Weights,
             Involatiles_h.Weights,
             MemoryRequired,
             cudaMemcpyHostToDevice);
  checkCUDAErrors(
      "Unable to copy Weights from host to device.", __FILE__, __LINE__);

  cudaMemcpy(Involatiles_d.Luminosities,
             Involatiles_h.Luminosities,
             MemoryRequired,
             cudaMemcpyHostToDevice);
  checkCUDAErrors(
      "Unable to copy Luminosities from host to device.", __FILE__, __LINE__);

  cudaMemcpy(Involatiles_d.Alphas,
             Involatiles_h.Alphas,
             Involatiles_h.nSpecies * MemoryRequired,
             cudaMemcpyHostToDevice);
  checkCUDAErrors(
      "Unable to copy Alphas from host to device.", __FILE__, __LINE__);

#if 0
printFrequencies<<<1,16>>>(Involatiles_d);
#endif

  return EXIT_SUCCESS;
}
