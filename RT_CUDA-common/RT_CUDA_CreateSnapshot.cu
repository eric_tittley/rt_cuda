#include "RT.h"
#include "RT_CUDA.h"
#include "libcudasupport.h"

/* Dumps RT data to a file */

int RT_CUDA_CreateSnapshot(RT_Data Data_host,
                           RT_Data Data_dev,
                           RT_RatesBlockT Rates_dev,
                           thrust::host_vector<unsigned int> &gadget_map,
                           rt_float Redshift,
                           rt_float t,
                           rt_float outputTimesUnit) {
  rt_float const ExpansionFactor = 1. / (Redshift + 1.);
  size_t const LABEL_STR_SIZE = 32;
  char RTFile[LABEL_STR_SIZE];
  rt_float const t_outputTimeUnits = t / outputTimesUnit;

  // Copy data from device
  cudaError_t cudaError = cudaMemcpy(Data_host.MemSpace,
                                     Data_dev.MemSpace,
                                     Data_host.NBytes,
                                     cudaMemcpyDeviceToHost);
  checkCudaAPICall(cudaError, __FILE__, __LINE__);

  sortRTDataIntoGadgetOrder(&Data_host, gadget_map);

  /* Dump data */
#if VERBOSE > 1
  std::cout << "Saving data...";
#endif
  int ierr;
  ierr = snprintf(
      RTFile, LABEL_STR_SIZE - 1, "%s/RTData_t=%07.3f", Dir, t_outputTimeUnits);
  if (ierr > (int)LABEL_STR_SIZE) {
    printf("ERROR: %s: %i: Path too long %s\n", __FILE__, __LINE__, RTFile);
    return EXIT_FAILURE;
  }
  RT_FileHeaderT FileHeader;
  FileHeader.ExpansionFactor = (float)ExpansionFactor;
  FileHeader.Redshift = (float)Redshift;
  FileHeader.Time = (float)t;
  RT_Data *PointerToRT_Data = &Data_host;
  RT_Data **PointerToArrayOfPointersToRT_Data = &PointerToRT_Data;
  RT_SaveData(RTFile, PointerToArrayOfPointersToRT_Data, 1, FileHeader);

  /* * Save Rates to file * */
  /*   Allocate host memory for Rates   */
  RT_RatesBlockT Rates_host;
  Rates_host.NumCells = Data_host.NumCells;
  ierr = RT_RatesBlock_Allocate(Data_host.NumCells, &Rates_host);
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Failed to allocate host memory for Rates\n",
           __FILE__,
           __LINE__);
    return EXIT_FAILURE;
  }
  /*   Copy Rates from device to host   */
  RT_CUDA_CopyRatesDeviceToHost(Rates_dev, Rates_host);
  sortRatesIntoGadgetOrder(&Rates_host, gadget_map);
  /*   Write Rates to file              */
  ierr = snprintf(
      RTFile, LABEL_STR_SIZE - 1, "%s/Rates_t=%07.3f", Dir, t_outputTimeUnits);
  if (ierr > (int)LABEL_STR_SIZE) {
    printf("ERROR: %s: %i: Path too long %s\n", __FILE__, __LINE__, RTFile);
    return EXIT_FAILURE;
  }
  FILE *fid = fopen(RTFile, "w");
  if (fid == NULL) {
    printf(
        "ERROR: %s: %i: Unable to open file %s\n", __FILE__, __LINE__, RTFile);
    ReportFileError(RTFile);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }
  ierr = RT_RatesBlock_Write(Rates_host, fid);
  if (ierr != EXIT_SUCCESS) {
    printf("ERROR: %s: %i: Failed to write Rates to file %s\n",
           __FILE__,
           __LINE__,
           RTFile);
    return EXIT_FAILURE;
  }
  /* Close the file */
  ierr = fclose(fid);
  if (ierr != 0) {
    printf("ERROR: %s: Unable to close file %s\n", __FILE__, RTFile);
    ReportFileError(RTFile);
    (void)fflush(stdout);
    return EXIT_FAILURE;
  }

  /*   Free local Rates memory          */
  RT_RatesBlock_Free(Rates_host);

  return EXIT_SUCCESS;
}
