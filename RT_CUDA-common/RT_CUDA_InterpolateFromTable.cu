/* Interpolate from a table.
 *
 * ARGUMENTS
 *  xp	The point at which to interpolate a value
 *  N	The length of the table
 *  x	The independent value in the table	(vector of length N)
 *  y	The dependent value in the table	(vector of length N)
 *
 * RETURNS
 *  y interpolated at xp
 *
 * AUTHOR: Eric Tittley
 */

#include "RT_CUDA.h"

/* Interpolate input spectrum from a table */
__device__ rt_float RT_CUDA_InterpolateFromTable(rt_float const xp,
                                                 int const N,
                                                 rt_float const* const x,
                                                 rt_float const* const y) {
  int i, i_lo = 0, i_hi;
  rt_float frac;

  /* Find the indices bracketing xp */
  /* First, try an inefficient algorithm */
  for (i = 1; i < N; i++) {
    if (xp <= x[i]) {
      i_lo = i - 1;
      break;
    }
  }
  i_hi = i_lo + 1;
  /* We will use an algorithm that is efficient if successive calls
   * have similar yp values. */
  /* NOT YET DONE */

  /* Now do a linear interpolation.  A logarithmic interpolation may be more
   * appropriate, but it will be substantially less efficient. */
  frac = (xp - x[i_lo]) / (x[i_hi] - x[i_lo]);
  return y[i_lo] + frac * (y[i_hi] - y[i_lo]);
}
