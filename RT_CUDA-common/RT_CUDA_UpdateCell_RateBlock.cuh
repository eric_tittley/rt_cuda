/* RT_CUDA_UpdateCell_RateBlock: Update Entropy and Ionization states for the
 * next iteration
 *
 * ierr = RT_CUDA_UpdateCell_RateBlock(const int me, rt_float const dt_RT,
 * rt_float const D, rt_float const ExpansionFactor, RT_Rates->BlockT *Rates->,
 * RT_Cell *Data_cell );
 *
 * ARGUMENTS INPUT, NOT MODIFIED
 *  me
 *  dt_RT
 *  n_e
 *  n_H
 *  n_He
 *  Rates->I_H1
 *  Rates->I_He1
 *  Rates->I_He2
 *  Rates->alpha_H1
 *  Rates->alpha_He1
 *  Rates->alpha_He2
 *  Rates->G
 *  Rates->L
 *  mu
 *  T_CMB
 *
 * ARGUMENTS INPUT AND MODIFIED
 *  n_H1
 *  n_H2
 *  n_He1
 *  n_He2
 *  n_He3
 *  Entropy
 *
 * ARGUMENTS INITIALISED
 *  f_*         Ionization fractions of various species.
 *
 * PREPROCESSOR MACROS
 *  ENFORCE_MINIMUM_CMB_TEMPERATURE
 *
 * AUTHOR: Eric Tittley
 */
#pragma once

#ifdef HAVE_CONFIG
#  include "config.h"
#else
#  define ENFORCE_MINIMUM_CMB_TEMPERATURE 2.0
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <nppdefs.h>
#  include <stdio.h>
#endif

#include "RT_CUDA.h"

inline __device__ void RT_CUDA_UpdateCell_RateBlock(
    rt_float const dt_RT,
    rt_float const ExpansionFactor,
    size_t const cellindex,
    RT_ConstantsT const *const Constants,
    RT_RatesBlockT const *const Rates,
    /* updated */ RT_Data *Data) {
  /* Find the neutral & ionised number densities. */
  rt_float n_H1 = Data->n_H[cellindex] * Data->f_H1[cellindex];
  rt_float n_H2 = Data->n_H[cellindex] * Data->f_H2[cellindex];
  rt_float n_He1 = Data->n_He[cellindex] * Data->f_He1[cellindex];
  rt_float n_He2 = Data->n_He[cellindex] * Data->f_He2[cellindex];
  rt_float n_He3 = Data->n_He[cellindex] * Data->f_He3[cellindex];

  /* Electron density */
  rt_float n_e = n_H2 + n_He2 + (2.0 * n_He3);

  /*Solves for Hydrogen species density */
  rt_float const n_H1new =
      n_H1 + dt_RT * (n_H2 * n_e * Rates->alpha_H1[cellindex] -
                      Rates->I_H1[cellindex]);
  rt_float const n_H2new =
      n_H2 + dt_RT * (Rates->I_H1[cellindex] -
                      n_H2 * n_e * Rates->alpha_H1[cellindex]);
  n_H1 = n_H1new;
  n_H2 = n_H2new;

  /* If
   *  H is mostly ionized
   *   OR
   *  element is mostly ionised AND HI is within 1% of equilibrium
   *   OR
   *  element is mostly neutral AND HI is within 1% of equilibrium
   * then set to the equilibrium value.
   *
   * SAME CONDITIONAL MUST EXIST IN the findMinimumTimescale() code. */
  if (RT_CUDA_useEquilibriumValues(cellindex, Data, Rates, Constants)) {
    n_H1 = Rates->n_HI_Equilibrium[cellindex];
    n_H2 = Rates->n_HII_Equilibrium[cellindex];
  }

  /* The following conditions should never really be triggered. */
  if ((n_H1 < 0.0) || (n_H2 > Data->n_H[cellindex])) {
    n_H1 = Rates->n_HI_Equilibrium[cellindex];
    n_H2 = Rates->n_HII_Equilibrium[cellindex];
  }
  if ((n_H1 > Data->n_H[cellindex]) || (n_H2 < 0)) {
    n_H1 = Data->n_H[cellindex];
    n_H2 = 0.;
  }

#ifdef DEBUG
  if ((!(bool)isfinite(n_H1)) || (n_H1 < -NPP_MINABS_32F)) {
    printf(
        "ERROR: %s: %i: cellindex=%lu; n_H1=%e; n_H1new=%5.3e; "
        "n_HI_Equilibrium=%5.3e;\n",
        __FILE__,
        __LINE__,
        cellindex,
        n_H1,
        n_H1new,
        Rates->n_HI_Equilibrium[cellindex]);
  }
#endif

  /*Solves for helium species densities */
  rt_float const n_He1new =
      n_He1 + dt_RT * (n_e * n_He2 * Rates->alpha_He1[cellindex] -
                       Rates->I_He1[cellindex]);
  rt_float const n_He2new =
      n_He2 + dt_RT * (Rates->I_He1[cellindex] - Rates->I_He2[cellindex] +
                       n_e * (n_He3 * Rates->alpha_He2[cellindex] -
                              n_He2 * Rates->alpha_He1[cellindex]));
  rt_float const n_He3new =
      n_He3 + dt_RT * (Rates->I_He2[cellindex] -
                       n_e * n_He3 * Rates->alpha_He2[cellindex]);
  n_He1 = n_He1new;
  n_He2 = n_He2new;
  n_He3 = n_He3new;

  if (n_He1 > Data->n_He[cellindex]) {
    n_He1 = Data->n_He[cellindex];
    n_He2 = 0.;
    n_He3 = 0.;
  }
  if (n_He2 > Data->n_He[cellindex]) {
    n_He1 = 0.;
    n_He2 = Data->n_He[cellindex];
    n_He3 = 0.;
  }
  if (n_He3 > Data->n_He[cellindex]) {
    n_He1 = 0.;
    n_He2 = 0.;
    n_He3 = Data->n_He[cellindex];
  }
  rt_float f;
  if (n_He1 < 0.) {
    n_He1 = 0.;
    if (n_He2 < n_He3) {
      f = n_He2 / (n_He2 + n_He3);
      n_He2 = f * Data->n_He[cellindex];
      n_He3 = (1. - f) * Data->n_He[cellindex];
    } else {
      f = n_He3 / (n_He2 + n_He3);
      n_He3 = f * Data->n_He[cellindex];
      n_He2 = (1. - f) * Data->n_He[cellindex];
    }
  }
  if (n_He2 < 0.) {
    n_He2 = 0.;
    if (n_He1 < n_He3) {
      f = n_He1 / (n_He1 + n_He3);
      n_He1 = f * Data->n_He[cellindex];
      n_He3 = (1. - f) * Data->n_He[cellindex];
    } else {
      f = n_He3 / (n_He1 + n_He3);
      n_He3 = f * Data->n_He[cellindex];
      n_He1 = (1. - f) * Data->n_He[cellindex];
    }
  }
  if (n_He3 < 0.) {
    n_He3 = 0.;
    if (n_He2 < n_He1) {
      f = n_He2 / (n_He2 + n_He1);
      n_He2 = f * Data->n_He[cellindex];
      n_He1 = (1. - f) * Data->n_He[cellindex];
    } else {
      f = n_He1 / (n_He2 + n_He1);
      n_He1 = f * Data->n_He[cellindex];
      n_He2 = (1. - f) * Data->n_He[cellindex];
    }
  }

  /* Update ionization fractions */
  if (Data->n_H[cellindex] > 0.) {
    Data->f_H1[cellindex] = n_H1 / Data->n_H[cellindex];
    Data->f_H2[cellindex] = n_H2 / Data->n_H[cellindex];
  } else {
    Data->f_H1[cellindex] = 1.;
    Data->f_H2[cellindex] = 0.;
  }
  /* remember: small = big - big leads to truncation errors.
   * Always perform the substraction: big = big - small */
  if (Data->f_H1[cellindex] > Data->f_H2[cellindex]) {
    Data->f_H1[cellindex] = 1. - Data->f_H2[cellindex];
  } else {
    Data->f_H2[cellindex] = 1. - Data->f_H1[cellindex];
  }
  if (Data->n_He[cellindex] > 0.) {
    Data->f_He1[cellindex] = n_He1 / Data->n_He[cellindex];
    Data->f_He2[cellindex] = n_He2 / Data->n_He[cellindex];
    Data->f_He3[cellindex] = n_He3 / Data->n_He[cellindex];
    if ((Data->f_He1[cellindex] >= Data->f_He2[cellindex]) &&
        (Data->f_He1[cellindex] >= Data->f_He3[cellindex])) {
      Data->f_He1[cellindex] =
          1. - Data->f_He2[cellindex] - Data->f_He3[cellindex];
    } else if ((Data->f_He2[cellindex] >= Data->f_He1[cellindex]) &&
               (Data->f_He2[cellindex] >= Data->f_He3[cellindex])) {
      Data->f_He2[cellindex] =
          1. - Data->f_He1[cellindex] - Data->f_He3[cellindex];
    } else {
      Data->f_He3[cellindex] =
          1. - Data->f_He1[cellindex] - Data->f_He2[cellindex];
    }
  } else { /* No Helium */
    Data->f_He1[cellindex] = 1.;
    Data->f_He2[cellindex] = 0.;
    Data->f_He3[cellindex] = 0.;
  }

#ifdef DEBUG
  if (!(bool)isfinite(Data->f_H1[cellindex]) || Data->f_H1[cellindex] < 0.0) {
    printf(
        "ERROR: %s: %i: f_H1[%lu]=%5.3e; n_e=%5.3e; alpha_H1=%5.3e; "
        "I_H1=%5.3e\n",
        __FILE__,
        __LINE__,
        cellindex,
        Data->f_H1[cellindex],
        n_e,
        Rates->alpha_H1[cellindex],
        Rates->I_H1[cellindex]);
    return;
  }
#endif

  /* Update the entropy */
  /* Need to worry about under & over flows.
   * Scale is 10^(9 - ( -24 * 5/3 ) + (-10 -17 ) )
   * Which has an overflow ( 9 + 40 ) and an underflow (-10 - 17).
   * So stick the +17 with the density as (-24 +17*3/5)*5/3
   * to make it 10^(9 - (-23) -10 )  which is float safe in any order. */
  rt_float ResidualHeatingScaleFactorGammaInv =
      POW(Constants->Ma, 1. / Constants->gamma_ad);
  rt_float const EntropyRate =
      ((Constants->gamma_ad - 1.) /
       POW(Data->Density[cellindex] * ResidualHeatingScaleFactorGammaInv,
           Constants->gamma_ad) *
       (Rates->G[cellindex] - Rates->L[cellindex]));
  rt_float Entropy_new = Data->Entropy[cellindex] + dt_RT * EntropyRate;
#ifdef DEBUG
  /* Particles at minimum temperature are allowed to cool below zero.
   * Otherwise time steps degrade massively. */
  if (!isfinite(Entropy_new)
#  if !defined(ENFORCE_MINIMUM_CMB_TEMPERATURE) && \
      !defined(CONSTANT_TEMPERATURE)
      || (Entropy_new <= NPP_MINABS_32F)
#  endif
  ) {
    printf(
        "ERROR: %s: %i: Entropy_new=%5.3e; Entropy=%5.3e; dt_RT=%5.3e; "
        "EntropyRate=%5.3e; Density=%5.3e; G=%5.3e; L=%5.3e;\n",
        __FILE__,
        __LINE__,
        Entropy_new,
        Data->Entropy[cellindex],
        dt_RT,
        EntropyRate,
        Data->Density[cellindex],
        Rates->G[cellindex],
        Rates->L[cellindex]);
    return;
  }

#  ifdef ENFORCE_MINIMUM_CMB_TEMPERATURE
  if (Entropy_new < RT_FLT_C(0.)) {
    printf(
        "INFO: %s, %i: time scale allowed particle to cool below zero\n"
        "    Entropy_new=%5.3e; Entropy=%5.3e; dt_RT=%5.3e; "
        "EntropyRate=%5.3e;\n",
        __FILE__,
        __LINE__,
        Entropy_new,
        Data->Entropy[cellindex],
        dt_RT,
        EntropyRate);
  }
#  endif
#endif

#ifdef ENFORCE_MINIMUM_CMB_TEMPERATURE
  /* The entropy cannot go below that set by the CMB temperature */
  rt_float const Entropy_min = RT_CUDA_EntropyFromTemperature(
      ENFORCE_MINIMUM_CMB_TEMPERATURE, cellindex, Data, Constants);
#else
  /* NOTE: This is bad.  Entropy can never be zero. The code will crash
   * elsewhere if Entropy is zero.  Need something to put here. */
  rt_float const Entropy_min = RT_FLT_C(0.0);
#endif

  if (Entropy_new < Entropy_min) {
    Entropy_new = Entropy_min;
  }

#if defined(CONSTANT_TEMPERATURE)
  Entropy_new = RT_CUDA_EntropyFromTemperature(
      CONSTANT_TEMPERATURE, cellindex, Data, Constants);
#endif

  Data->Entropy[cellindex] = Entropy_new;

  Data->T[cellindex] =
      RT_CUDA_TemperatureFromEntropy(Entropy_new, cellindex, Data, Constants);
#ifdef DEBUG
  if ((!(bool)isfinite(Data->T[cellindex])) ||
      (Data->T[cellindex] < NPP_MINABS_32F)) {
    printf("ERROR: %s: %i: Temperature=%e: Entropy=%5.3e\n",
           __FILE__,
           __LINE__,
           Data->T[cellindex],
           Data->Entropy[cellindex]);
  }
#endif
}
