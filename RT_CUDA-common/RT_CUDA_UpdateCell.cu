/* RT_UpdateCell: Update Entropy and Ionization states for the next iteration
 *
 * ierr = RT_UpdateCell(const int me, rt_float const dt_RT, rt_float const D,
 *                      rt_float const ExpansionFactor, RT_RatesT *Rates,
 * RT_Cell *Data_cell );
 *
 * ARGUMENTS INPUT, NOT MODIFIED
 *  me
 *  dt_RT
 *  n_e
 *  n_H
 *  n_He
 *  Rates->I_H1
 *  Rates->I_He1
 *  Rates->I_He2
 *  Rates->alpha_H1
 *  Rates->alpha_He1
 *  Rates->alpha_He2
 *  G
 *  L
 *  T_CMB
 *
 * ARGUMENTS INPUT AND MODIFIED
 *  n_H1
 *  n_H2
 *  n_He1
 *  n_He2
 *  n_He3
 *  Entropy
 *
 * ARGUMENTS INITIALISED
 *  f_*         Ionization fractions of various species.
 *
 * PREPROCESSOR MACROS
 *  ENFORCE_MINIMUM_CMB_TEMPERATURE
 *
 * AUTHOR: Eric Tittley
 */

#ifdef HAVE_CONFIG
#  include "config.h"
#else
#  define ENFORCE_MINIMUM_CMB_TEMPERATURE 2.0
#endif

#include <stdlib.h>
#ifdef DEBUG
#  include <stdio.h>
#endif

#include "RT_CUDA.h"

__device__ void RT_CUDA_UpdateCell(rt_float const dt_RT,
                                   rt_float const ExpansionFactor,
                                   size_t const cellindex,
                                   RT_ConstantsT const* const Constants,
                                   RT_RatesT const* const Rates,
                                   RT_Data* const Data_dev) {
  /* Find the neutral & ionised number densities. */
  rt_float n_H1 = Data_dev->n_H[cellindex] * Data_dev->f_H1[cellindex];
  rt_float n_H2 = Data_dev->n_H[cellindex] * Data_dev->f_H2[cellindex];
  rt_float n_He1 = Data_dev->n_He[cellindex] * Data_dev->f_He1[cellindex];
  rt_float n_He2 = Data_dev->n_He[cellindex] * Data_dev->f_He2[cellindex];
  rt_float n_He3 = Data_dev->n_He[cellindex] * Data_dev->f_He3[cellindex];

  /* Electron density */
  rt_float n_e = n_H2 + n_He2 + (2.0 * n_He3);

  /*Solves for Hydrogen species density */
  rt_float const n_H1new =
      n_H1 + dt_RT * (n_H2 * n_e * Rates->alpha_H1 - Rates->I_H1);
  rt_float const n_H2new =
      n_H2 + dt_RT * (Rates->I_H1 - n_H2 * n_e * Rates->alpha_H1);
  n_H1 = n_H1new;
  n_H2 = n_H2new;

  /* The following conditions should never really be triggered. */
  if ((n_H1 < 0) || (n_H2 > Data_dev->n_H[cellindex])) {
    n_H1 = 0.;
    n_H2 = Data_dev->n_H[cellindex];
  }
  if ((n_H1 > Data_dev->n_H[cellindex]) || (n_H2 < 0)) {
    n_H1 = Data_dev->n_H[cellindex];
    n_H2 = 0.;
  }

  /*Solves for helium species densities */
  rt_float const n_He1new =
      n_He1 + dt_RT * (n_e * n_He2 * Rates->alpha_He1 - Rates->I_He1);
  rt_float const n_He2new =
      n_He2 +
      dt_RT * (Rates->I_He1 - Rates->I_He2 +
               n_e * (n_He3 * Rates->alpha_He2 - n_He2 * Rates->alpha_He1));
  rt_float const n_He3new =
      n_He3 + dt_RT * (Rates->I_He2 - n_e * n_He3 * Rates->alpha_He2);
  n_He1 = n_He1new;
  n_He2 = n_He2new;
  n_He3 = n_He3new;

  if (n_He1 > Data_dev->n_He[cellindex]) {
    n_He1 = Data_dev->n_He[cellindex];
    n_He2 = 0.;
    n_He3 = 0.;
  }
  if (n_He2 > Data_dev->n_He[cellindex]) {
    n_He1 = 0.;
    n_He2 = Data_dev->n_He[cellindex];
    n_He3 = 0.;
  }
  if (n_He3 > Data_dev->n_He[cellindex]) {
    n_He1 = 0.;
    n_He2 = 0.;
    n_He3 = Data_dev->n_He[cellindex];
  }
  rt_float f;
  if (n_He1 < 0.) {
    n_He1 = 0.;
    if (n_He2 < n_He3) {
      f = n_He2 / (n_He2 + n_He3);
      n_He2 = f * Data_dev->n_He[cellindex];
      n_He3 = (1. - f) * Data_dev->n_He[cellindex];
    } else {
      f = n_He3 / (n_He2 + n_He3);
      n_He3 = f * Data_dev->n_He[cellindex];
      n_He2 = (1. - f) * Data_dev->n_He[cellindex];
    }
  }
  if (n_He2 < 0.) {
    n_He2 = 0.;
    if (n_He1 < n_He3) {
      f = n_He1 / (n_He1 + n_He3);
      n_He1 = f * Data_dev->n_He[cellindex];
      n_He3 = (1. - f) * Data_dev->n_He[cellindex];
    } else {
      f = n_He3 / (n_He1 + n_He3);
      n_He3 = f * Data_dev->n_He[cellindex];
      n_He1 = (1. - f) * Data_dev->n_He[cellindex];
    }
  }
  if (n_He3 < 0.) {
    n_He3 = 0.;
    if (n_He2 < n_He1) {
      f = n_He2 / (n_He2 + n_He1);
      n_He2 = f * Data_dev->n_He[cellindex];
      n_He1 = (1. - f) * Data_dev->n_He[cellindex];
    } else {
      f = n_He1 / (n_He2 + n_He1);
      n_He1 = f * Data_dev->n_He[cellindex];
      n_He2 = (1. - f) * Data_dev->n_He[cellindex];
    }
  }

  /* Update ionization fractions */
  if (Data_dev->n_H[cellindex] > 0.) {
    Data_dev->f_H1[cellindex] = n_H1 / Data_dev->n_H[cellindex];
    Data_dev->f_H2[cellindex] = n_H2 / Data_dev->n_H[cellindex];
  } else {
    Data_dev->f_H1[cellindex] = 1.;
    Data_dev->f_H2[cellindex] = 0.;
  }
  /* remember: small = big - big leads to truncation errors.
   * Always perform the substraction: big = big - small */
  if (Data_dev->f_H1[cellindex] > Data_dev->f_H2[cellindex]) {
    Data_dev->f_H1[cellindex] = 1. - Data_dev->f_H2[cellindex];
  } else {
    Data_dev->f_H2[cellindex] = 1. - Data_dev->f_H1[cellindex];
  }
  if (Data_dev->n_He[cellindex] > 0.) {
    Data_dev->f_He1[cellindex] = n_He1 / Data_dev->n_He[cellindex];
    Data_dev->f_He2[cellindex] = n_He2 / Data_dev->n_He[cellindex];
    Data_dev->f_He3[cellindex] = n_He3 / Data_dev->n_He[cellindex];
    if ((Data_dev->f_He1[cellindex] >= Data_dev->f_He2[cellindex]) &&
        (Data_dev->f_He1[cellindex] >= Data_dev->f_He3[cellindex])) {
      Data_dev->f_He1[cellindex] =
          1. - Data_dev->f_He2[cellindex] - Data_dev->f_He3[cellindex];
    } else if ((Data_dev->f_He2[cellindex] >= Data_dev->f_He1[cellindex]) &&
               (Data_dev->f_He2[cellindex] >= Data_dev->f_He3[cellindex])) {
      Data_dev->f_He2[cellindex] =
          1. - Data_dev->f_He1[cellindex] - Data_dev->f_He3[cellindex];
    } else {
      Data_dev->f_He3[cellindex] =
          1. - Data_dev->f_He1[cellindex] - Data_dev->f_He2[cellindex];
    }
  } else { /* No Helium */
    Data_dev->f_He1[cellindex] = 1.;
    Data_dev->f_He2[cellindex] = 0.;
    Data_dev->f_He3[cellindex] = 0.;
  }

  /* Update the entropy */
  /* Need to worry about under & over flows.
   * Scale is 10^(9 - ( -24 * 5/3 ) + (-10 -17 ) )
   * Which has an overflow ( 9 + 40 ) and an underflow (-10 - 17).
   * So stick the +17 with the density as (-24 +17*3/5)*5/3
   * to make it 10^(9 - (-23) -10 )  which is float safe in any order. */
  rt_float ResidualHeatingScaleFactorGammaInv =
      POW(Constants->Ma, 1. / Constants->gamma_ad);
  rt_float Entropy_new =
      Data_dev->Entropy[cellindex] +
      dt_RT * (Constants->gamma_ad - 1.) /
          POW(Data_dev->Density[cellindex] * ResidualHeatingScaleFactorGammaInv,
              Constants->gamma_ad) *
          (Rates->G - Rates->L);

#ifdef DEBUG
  if (!(bool)isfinite(Entropy_new)) {
    printf(
        "ERROR: %s: %i: Entropy_new=%5.3e; Entropy=%5.3e; dt_RT=%5.3e; "
        "Density=%5.3e; G=%5.3e; L=%5.3e;\n",
        __FILE__,
        __LINE__,
        Entropy_new,
        Data_dev->Entropy[cellindex],
        dt_RT,
        Data_dev->Density[cellindex],
        Rates->G,
        Rates->L);
  }
#endif

  /* Update the neutral & ionised number densities. */
  n_H1 = Data_dev->n_H[cellindex] * Data_dev->f_H1[cellindex];
  n_H2 = Data_dev->n_H[cellindex] * Data_dev->f_H2[cellindex];
  n_He1 = Data_dev->n_He[cellindex] * Data_dev->f_He1[cellindex];
  n_He2 = Data_dev->n_He[cellindex] * Data_dev->f_He2[cellindex];
  n_He3 = Data_dev->n_He[cellindex] * Data_dev->f_He3[cellindex];

  /* Electron density */
  n_e = n_H2 + n_He2 + (2. * n_He3);

#if (defined ENFORCE_MINIMUM_CMB_TEMPERATURE) || (defined CONSTANT_TEMPERATURE)
#  ifdef ENFORCE_MINIMUM_CMB_TEMPERATURE
  /* The entropy cannot go below that set by the CMB temperature */
  rt_float const Entropy_Min = RT_CUDA_EntropyFromTemperature(
      ENFORCE_MINIMUM_CMB_TEMPERATURE, cellindex, Data_dev, Constants);
  if (Entropy_new < Entropy_Min) {
    Entropy_new = Entropy_Min;
  }
#  else
  /* CONSTANT_TEMPERATURE */
  Entropy_new = RT_CUDA_EntropyFromTemperature(
      CONSTANT_TEMPERATURE, cellindex, Data_dev, Constants);
#  endif
#  ifdef DEBUG
  if (!(bool)isfinite(Entropy_new) || (Entropy_new <= 0)) {
    printf("ERROR: %s: %i: Entropy_new=%5.3e; Density=%5.3e\n",
           __FILE__,
           __LINE__,
           Entropy_new,
           Data_dev->Density[cellindex]);
  }
#  endif
#endif
  if (Entropy_new < 0.) {
    /* NOTE: This is bad.  Entropy can never be zero. The code will crash
     * elsewhere if Entropy is zero.  Need something to put here. */
    Entropy_new = 0;
  }

  Data_dev->Entropy[cellindex] = Entropy_new;

  Data_dev->T[cellindex] = RT_CUDA_TemperatureFromEntropy(
      Entropy_new, cellindex, Data_dev, Constants);
}
