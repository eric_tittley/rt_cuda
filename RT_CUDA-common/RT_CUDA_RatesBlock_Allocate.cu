/*
 * ARGUMENTS
 *  nParticles
 *
 * RETURNS
 *  RT_RatesBlockT structure on the host whose elements are pointers to
 *  global memory spaces on the device.
 *    rt_float *I_H1
 *    rt_float *I_He1
 *    rt_float *I_He2
 *    rt_float *G_H1
 *    rt_float *G_He1
 *    rt_float *G_He2
 */
#include <cuda.h>
#include <stdio.h>

#include "RT_CUDA.h"

RT_RatesBlockT RT_CUDA_RatesBlock_Allocate(size_t const nParticles) {
  RT_RatesBlockT Rates_dev;
  cudaError_t error;

  error = cudaMalloc(&(Rates_dev.I_H1), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.I_He1), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.I_He2), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.G), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.G_H1), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.G_He1), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.G_He2), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.alpha_H1), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.alpha_He1), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.alpha_He2), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.L), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.Gamma_HI), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error =
      cudaMalloc(&(Rates_dev.n_HI_Equilibrium), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error =
      cudaMalloc(&(Rates_dev.n_HII_Equilibrium), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }
  error = cudaMalloc(&(Rates_dev.TimeScale), nParticles * sizeof(rt_float));
  if (error != cudaSuccess) {
    printf(
        "ERROR: %s: %i: ERROR: Unable to allocate %lu bytes on CUDA device\n",
        __FILE__,
        __LINE__,
        nParticles * sizeof(rt_float));
  }

  return Rates_dev;
  /* Rates_dev gets destroyed on exit, but the memory allocated remains
   * allocated and the copy of Rates_dev returned has the correct pointer
   * values. */
}
