/* RT_CUDA_FreeConstantsOnDevice: Free device memory allocations in support of
 * RT_ConstantsT
 *
 * int RT_CUDA_FreeConstantsOnDevice(RT_ConstantsT * Constants_dev);
 *
 * ARGUMENT
 *  Constants_dev	RT_ConstantsT structure with pointers to cudaMalloc'ed memory.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *
 * SEE ALSO
 *  RT_CUDA_CopyConstantsToDevice
 *
 * AUTHOR: Eric Tittley
 */

#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <stdlib.h>

#include "RT_CUDA.h"
#include "cudasupport.h"

int RT_CUDA_FreeConstantsOnDevice(RT_ConstantsT const Constants,
                                  RT_ConstantsT *Constants_dev) {
  /* NLevels */
  cudaFree((void *)(Constants_dev->NLevels));
  checkCUDAErrors("cudaFree experienced an error", __FILE__, __LINE__);

  return EXIT_SUCCESS;
}
