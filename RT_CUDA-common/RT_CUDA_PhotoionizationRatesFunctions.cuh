#pragma once
#include "Epsilon.h"
#include "RT_CUDA.h"

#ifdef DEBUG
#  include <nppdefs.h>
#endif

/* This function assumes 1 frequency per thread, with L(nu, sourceRedshift)
   precalculated. Same as before, Alpha[3] has been precalculated at nu.  */
inline __device__ RT_CUDA_PhotoRates
RT_CUDA_PhotoionizationRatesFunctions(rt_float const nu,
                                      rt_float const L_unattenuated,
                                      rt_float const N_H1,
                                      rt_float const N_He1,
                                      rt_float const N_He2,
                                      rt_float const N_H1_cum,
                                      rt_float const N_He1_cum,
                                      rt_float const N_He2_cum,
                                      RT_ConstantsT const* const Constants,
                                      rt_float const* const Alpha) {
  rt_float tau_1, tau_2, tau_3, tau;
  rt_float Trans, norm;
  rt_float TransmissionThroughCell;

  /* k_i   = The photoionization cross sections
   * tau_i = The optical depth of the gas in the cell at a given frequency.
   * e_i   = The exponentials, which are expensive to calculate. */
  tau_1 = Alpha[0] * N_H1;
  tau_2 = 0.;
  tau_3 = 0.;
  if (nu > Constants->nu_1) {
    tau_2 = Alpha[1] * N_He1;
    if (nu > Constants->nu_2) {
      tau_3 = Alpha[2] * N_He2;
    }
  }
#ifdef DEBUG
  if ((!isfinite(tau_1)) || (tau_1 < -NPP_MINABS_32F)) {
    printf("%s: %i: ERROR: tau_1=%5.3e; Alpha[0]=%5.3e; N_H1=%5.3e\n",
           __FILE__,
           __LINE__,
           tau_1,
           Alpha[0],
           N_H1);
    return {0};
  }
  if ((!isfinite(tau_2)) || (tau_2 < -NPP_MINABS_32F)) {
    printf("%s: %i: ERROR: tau_2=%e;\n", __FILE__, __LINE__, tau_2);
    return {0};
  }
  if ((!isfinite(tau_3)) || (tau_3 < -NPP_MINABS_32F)) {
    printf("%s: %i: ERROR: tau_3=%e;\n", __FILE__, __LINE__, tau_3);
    return {0};
  }
#endif

#ifdef DEBUG
  if ((!isfinite(N_H1_cum)) || (N_H1_cum < -NPP_MINABS_32F)) {
    printf("%s: %i: ERROR: N_H1_cum=%5.3e;\n", __FILE__, __LINE__, N_H1_cum);
  }
#endif

  /*The transmission factor.  What fraction of light has reached this cell. */
  Trans =
      EXP(-(Alpha[0] * N_H1_cum + Alpha[1] * N_He1_cum + Alpha[2] * N_He2_cum));

  /* Normalization */
  tau = tau_1 + tau_2 + tau_3;
  /* Edit: See notes 2014Mar28
   * (1.0 - EXP(-tau)) / tau Taylor Expands to 1-tau/2 for tau -> 0
   * Indeed, it is a very good approximation for tau < 0.01 (2x10^-5 fractional
   * error)
   */
  if (tau > RT_FLT_C(0.01)) {   /* Avoid expensive EXP() call and possible
                                   numerical errors */
    if (tau > INV_LN_EPSILON) { /* -ln(EPSILON) */
      TransmissionThroughCell = RT_FLT_C(1.0);
    } else {
      TransmissionThroughCell = RT_FLT_C(1.0) - EXP(-tau);
    }
    norm = TransmissionThroughCell / tau;
  } else {
    norm = RT_FLT_C(1.0) - tau / RT_FLT_C(2.0);
  }

  /* Convert the source's transmited absorbed luminosity at this frequency */
  rt_float const L = L_unattenuated * Trans * norm;
#ifdef DEBUG
  if ((!isfinite(L)) || (L < -NPP_MINABS_32F)) {
    printf("%s: %i: ERROR: L=%5.3e; Trans=%5.3e; norm=%5.3e\n",
           __FILE__,
           __LINE__,
           L,
           Trans,
           norm);
  }
#endif

  RT_CUDA_PhotoRates Rates{0};
  if (nu >= Constants->nu_0) {
    Rates.I_H1 = L * tau_1;
    Rates.G_H1 = Rates.I_H1 * (nu - Constants->nu_0);
    Rates.Gamma_HI = L * Alpha[0];
  }
  if (Constants->Y >
      RT_FLT_C(0.)) { /* Only evaluate I_He* and G_He* if there is He */
    if (nu >= Constants->nu_1) {
      Rates.I_He1 = L * tau_2;
      Rates.G_He1 = Rates.I_He1 * (nu - Constants->nu_1);
    }
    if (nu >= Constants->nu_2) {
      Rates.I_He2 = L * tau_3;
      Rates.G_He2 = Rates.I_He2 * (nu - Constants->nu_2);
    }
  }
#ifdef DEBUG
  rt_float f[7] = {Rates.I_H1,
                   Rates.I_He1,
                   Rates.I_He2,
                   Rates.G_H1,
                   Rates.G_He1,
                   Rates.G_He2,
                   Rates.Gamma_HI};
  for (size_t i = 0; i < 7; i++) {
    if ((!isfinite(f[i])) || (f[i] < -NPP_MINABS_32F)) {
      printf(
          "%s: %i: ERROR: f[%lu]=%e; L=%e; nu=%e; Trans=%5.3e; norm=%5.3e; "
          "Alpha[0]=%5.3e; N_H1_cum=%5.3e; tau_1=%5.3e; tau_2=%5.3e; "
          "tau_3=%5.3e\n",
          __FILE__,
          __LINE__,
          i,
          f[i],
          L,
          nu,
          Trans,
          norm,
          Alpha[0],
          N_H1_cum,
          tau_1,
          tau_2,
          tau_3);
      return Rates;
    }
  }
#endif
  return Rates;
}
