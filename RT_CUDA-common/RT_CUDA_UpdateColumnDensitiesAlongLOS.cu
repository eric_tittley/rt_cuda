#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include <stdlib.h>

#include "RT_CUDA.h"

/* Need to implement a parallel cumulative sum algorithm.
 * This is known in CS as a "prefix sum" or "all-prefix-sums operation",#
 * or "sum scan".  It can be generalized to any operator, $, such that
 * A=[a1 a2 a3 ... aN] goes to B=[a1 a1$a2 a1$a2$a3 ... (a1$a2$a3...$aN-1$aN)]
 * A condition for implementation on a GPU is that is all fit in a thread block,
 * so B=cumSum(A) should go to
 *  B1=cumSum(A(1:Nblock);
 *  B2=cumSum([B1(Nblock) A(Nblock+1:2*Nblock-1)]);
 *  B3= ...
 * http://http.developer.nvidia.com/GPUGems3/gpugems3_ch39.html
 *
 * I've written
 *  RT_CUDA_CumulativeSum.c
 * It is not the highly-optimized routine suggested above, but it is a simple
 * starting point.
 */

__device__ void RT_CUDA_UpdateColumnDensitiesAlongLOS(
    size_t const losoffset,
    rt_float const t_step,
    rt_float const dt,
    RT_ConstantsT const* const Constants,
    RT_Data const* const Data_dev) {
  /* Update the cumulative optical depths to the cells */
#ifndef THIN_APPROX
  /* Calculates column density up to the shell denoted by index iz.*/
#  if (defined REFINEMENTS) || (defined SEGMENTED_LOS)
  /* Linearly interpolate the cumulative column density incoming to the start
   * of the LOS.
   * NCol[0:5] must be set prior to calling RT_ProcessLOS.*/
  Data_dev->column_H1[losoffset + 0] =
      Data_dev->NCol[losoffset + 0] +
      t_step / dt *
          (Data_dev->NCol[losoffset + 3] - Data_dev->NCol[losoffset + 0]);
  Data_dev->column_He1[losoffset + 0] =
      Data_dev->NCol[losoffset + 1] +
      t_step / dt *
          (Data_dev->NCol[losoffset + 4] - Data_dev->NCol[losoffset + 1]);
  Data_dev->column_He2[losoffset + 0] =
      Data_dev->NCol[losoffset + 2] +
      t_step / dt *
          (Data_dev->NCol[losoffset + 5] - Data_dev->NCol[losoffset + 2]);
#  else
  Data_dev->column_H1[losoffset + 0] = 0.;
  Data_dev->column_He1[losoffset + 0] = 0.;
  Data_dev->column_He2[losoffset + 0] = 0.;
#  endif

  /* Each cell's column density is set to be the sum of the previous cells' */
  size_t iz;
  for (iz = 1; iz < Data_dev->NumCells; iz++) {
    Data_dev->column_H1[losoffset + iz] =
        Data_dev->column_H1[losoffset + iz - 1] +
        (Data_dev->n_H[losoffset + iz - 1] *
         Data_dev->f_H1[losoffset + iz - 1] * Data_dev->dR[losoffset + iz - 1]);
    Data_dev->column_He1[losoffset + iz] =
        Data_dev->column_He1[losoffset + iz - 1] +
        (Data_dev->n_He[losoffset + iz - 1] *
         Data_dev->f_He1[losoffset + iz - 1] *
         Data_dev->dR[losoffset + iz - 1]);
    Data_dev->column_He2[losoffset + iz] =
        Data_dev->column_He2[losoffset + iz - 1] +
        (Data_dev->n_He[losoffset + iz - 1] *
         Data_dev->f_He2[losoffset + iz - 1] *
         Data_dev->dR[losoffset + iz - 1]);
  }
#else
  /* Thin approximation.  The input spectrum is not modified by
   * absorption in the column. */
  size_t iz;
  for (iz = 0; iz < Data_dev->NumCells; iz++) {
    Data_dev->column_H1[losoffset + iz] = 0.;
    Data_dev->column_He1[losoffset + iz] = 0.;
    Data_dev->column_He2[losoffset + iz] = 0.;
  }
#endif

  return;
}
