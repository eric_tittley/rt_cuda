#include "RT_CUDA.h"

__device__ void print_cell(RT_Cell const *const Data_cell) {
  printf(
      "START CELL:\n"
      "Data_cell->R: %f\n"
      "Data_cell->Density: %f\n"
      "Data_cell->Entropy: %f\n"
      "Data_cell->T: %f\n"
      "Data_cell->n_H: %f\n"
      "Data_cell->f_H1: %f\n"
      "Data_cell->f_H2: %f\n"
      "Data_cell->n_He: %f\n"
      "Data_cell->f_He1: %f\n"
      "Data_cell->f_He2: %f\n"
      "Data_cell->f_He3: %f\n"
      "Data_cell->column_H1: %f\n"
      "Data_cell->column_He1: %f\n"
      "Data_cell->column_He2: %f\n"
      "FINISH CELL:\n",
      Data_cell->R,
      Data_cell->Density,
      Data_cell->Entropy,
      Data_cell->T,
      Data_cell->n_H,
      Data_cell->f_H1,
      Data_cell->f_H2,
      Data_cell->n_He,
      Data_cell->f_He1,
      Data_cell->f_He2,
      Data_cell->f_He3,
      Data_cell->column_H1,
      Data_cell->column_He1,
      Data_cell->column_He2);
}

__device__ void print_rates(RT_RatesT *Rates) {
  printf(
      "START RATES:\n"
      "I_H1: %e\n"
      "I_He1: %e\n"
      "I_He2: %e\n"
      "G: %e\n"
      "G_H1: %e\n"
      "G_He1: %e\n"
      "G_He2: %e\n"
      "alpha_H1: %e\n"
      "alpha_He1: %e\n"
      "alpha_He2: %e\n"
      "L: %e\n"
      "FINISH RATES:\n",
      Rates->I_H1,
      Rates->I_He1,
      Rates->I_He2,
      Rates->G,
      Rates->G_H1,
      Rates->G_He1,
      Rates->G_He2,
      Rates->alpha_H1,
      Rates->alpha_He1,
      Rates->alpha_He2,
      Rates->L
  );
}
