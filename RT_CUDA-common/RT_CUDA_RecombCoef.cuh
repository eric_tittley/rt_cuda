/* recomb_H1, recomb_He1, recomb_He2		 Recombination coefficients.
 * recombcool_H1, recombcool_He1, recombcool_He2 Cooling coefficients.
 *
 * rt_float RT_CUDA_recomb_H1(rt_float const T, int const recomb_approx)
 * rt_float RT_CUDA_recomb_He1(rt_float const T, int const recomb_approx)
 * rt_float RT_CUDA_recomb_He2(rt_float const T, int const recomb_approx)
 * rt_float RT_CUDA_recombcool_H1(rt_float const T, int const recomb_approx)
 * rt_float RT_CUDA_recombcool_He1(rt_float const T, int const recomb_approx)
 * rt_float RT_CUDA_recombcool_He2(rt_float const T, int const recomb_approx)
 *
 * ARGUMENTS
 *  Input, not modified
 *   T	Temperature [K]
 *
 * RETURNS
 *  Recombination coefficients. [m^3 s^-1]
 *  Recombination cooling coefficients. [J m^3 s^-1]
 *
 * AUTHOR: Jamie Bolton with modifications by Eric Tittley.
 */

/* #include <math.h> */
#pragma once
#include "RT_CUDA.h"
#include "RecombApprox.h"

/* H2-->H1 recombination coefficient*/
inline __device__ rt_float RT_CUDA_recomb_H1(rt_float const T,
                                             int const recomb_approx) {
  rt_float sqrtT = SQRT(T);
  rt_float logT = LOG(T);
  rt_float cbrtT = CBRT(T);
  /*H2-->H1 recombination coefficient, CGS units converted to SI units, m^3s^-1
   */
  if (recomb_approx == Approx_A) {
    /* Seaton, MNRAS 119, 81 (1959) */
    return 2.065e-17 / sqrtT * (6.414 - logT / 2. + 8.68e-3 * cbrtT);
  } else { /* Case B */
    /* From Avery Meiksin's Code */
    return 2.065e-17 / sqrtT *
           (5.62 - logT / 2. + 8.68e-3 * cbrtT + 2.01e-5 * POW(T, 0.8));
  }
}

/* He2-->He1 recombination coefficient*/
inline __device__ rt_float RT_CUDA_recomb_He1(rt_float const T,
                                              int const recomb_approx) {
  rt_float alpha_r, alpha_d;
  rt_float E1, E2;
  rt_float logT;
  rt_float Term1, Term2;
  /* rt_float alpha_A, a_0, a_1; */
  /* He2-->He1 recombination coefficient [m^3 s^-1]*/

  /* Dielectronic term. Aldrovandi & Pequignot 1973 A&Ap 25 137 */
  if (T > 25000.0) {
    E1 = EXP(-94000.0 / T);
    E2 = EXP(-470000.0 / T);
    alpha_d = 1.9e-9 * (1.0 + (0.3 * E1)) * E2 / (T * SQRT(T));
  } else {
    alpha_d = 0.0;
  }

  /* Radiative term. */
  if (recomb_approx == Approx_A) {
    /*  Verner & Ferland 1996 ApJS 103 467 */
    /*
    a_0 = POW((T / 15.54), 0.5);
    a_1 = POW((T / 3.676e7), 0.5);
    alpha_r =   3.294e-17 / (a_0 * POW((1 + a_0), 0.309) * POW((1 +
    a_1), 1.691));
    */
    /* My fit to Hummer & Storey 1998, MNRAS 297, 1073
     * Good for T < 25000 K */
    logT = LOG(T);
    Term1 = POW(T, -0.5526);
    Term2 = EXP(-0.006875 * logT * logT);
    alpha_r = 1.27e-16 * Term1 * Term2;
  } else { /* Case B */
    /* From Avery Meiksin's code. VF96 - alpha_1 */
    /*
    a_0 = POW((T / 15.54), 0.5);
    a_1 = POW((T / 3.676e7), 0.5);
    alpha_r =   3.294e-17 / (a_0 * POW((1 + a_0), 0.309) * POW((1 +
    a_1), 1.691))
              - 1.32e-17*POW(T,-0.48);
    */
    /* My fit to Hummer & Storey 1998, MNRAS 297, 1073
     * Good for T < 25000 K */
    logT = LOG(T);
    Term1 = POW(T, -0.5095);
    Term2 = EXP(-0.01429 * logT * logT);
    alpha_r = 1.007e-16 * Term1 * Term2;
  }

  return alpha_r + alpha_d;
}

/* He3-->He2 recombination coefficient*/
inline __device__ rt_float RT_CUDA_recomb_He2(rt_float const T,
                                              int const recomb_approx) {
  rt_float sqrtT, logT, cbrtT;
  sqrtT = SQRT(T);
  logT = LOG(T);
  cbrtT = CBRT(T);
  /*He3-->He2 recombination coefficient, CGS units converted to SI units */
  if (recomb_approx == Approx_A) {
    return 8.260e-17 / sqrtT * (7.107 - logT / 2. + 5.47e-3 * cbrtT);
  } else { /* Case B */
    /* From Avery Meiksin's code */
    return 8.260e-17 / sqrtT *
           (6.32 - logT / 2. + 5.47e-3 * cbrtT + 6.64e-6 * POW(T, 0.8));
  }
}

/******* RECOMBINATION COOLING COEFFICIENTS ******/

/* H2-->H1 recombination cooling coefficient, beta_H1 */
inline __device__ rt_float RT_CUDA_recombcool_H1(rt_float const T,
                                                 int const recomb_approx) {
  rt_float pow1, pow2, pow3;
  /* Recombination cooling coefficient, J m^3 s^-1 */
  if (recomb_approx == Approx_A) {
    pow1 = POW(T, -0.965);
    pow2 = POW(T, -0.502);
    pow3 = POW(1 + 784.4 * pow2, -2.697);
    /* Sea;ton 1959 MNRAS 119 81 (multiplied by kT) */
    /* return 2.851e-40*SQRT(T)*(5.914 - LOG(T)/2. + 0.01184  * CBRT(T)); */
    /* HG97 is better for T>5x10^4 */
    return 1.137e-31 * pow1 * pow3;
  } else { /* Case B */
    pow1 = POW(T, -0.970);
    pow2 = POW(T, -0.376);
    pow3 = POW(1 + 86.15 * pow2, -3.720);
    /* Hui & Gnedin 1997 MNRAS 292, 27 */
    return 2.34e-32 * pow1 * pow3;
  }
}

/* He2-->He1 recombination cooling coefficient, beta_He1 */
inline __device__ rt_float RT_CUDA_recombcool_He1(rt_float const T,
                                                  int const recomb_approx) {
  rt_float beta_r, beta_d;
  rt_float exp1, exp2;
  rt_float pow1, pow2;

  /* Dielectronic term [J m^3 s^-1]. Take alpha_d from
   * Aldrovandi & Pequignot 1973 A&Ap 25 137 (as used in recomb_He1() ) and
   * multiply by 3 ryd (1 ryd = 2.180e-18 J) according to
   * Gould & Thakur 1970 Annals of Physics 61, 351 */
  if (T > 25000.0) {
    exp1 = EXP(-94000.0 / T);
    exp2 = EXP(-470000.0 / T);
    beta_d = 1.24e-26 * (1.0 + (0.3 * exp1)) * exp2 / (T * SQRT(T));
  } else {
    beta_d = 0.0;
  }

  if (recomb_approx == Approx_A) {
    /* Radiative term. [J m^3 s^-1] From Black 1981 MNRAS 197, 553*/
    /* beta_r = 1.55e-39 * POW(T, 0.3647); */
    /* My fit to Hummer & Storey 1998 MNRAS 297, 1073
     * Valid from 10 K < T < 25000 K */
    pow1 = POW(T, 0.5971);
    pow2 = POW(T / 2.29, 0.3037);
    beta_r = 2.80e-39 * pow1 / (1. + pow2);
  } else { /* Case B */
    /* My fit to Hummer & Storey 1998 MNRAS 297, 1073
     * Valid from 10 K < T < 25000 K */
    pow1 = POW(T, 0.402);
    pow2 = POW(T / 5620, 0.444);
    beta_r = 1.53e-39 * pow1 / (1. + pow2);
  }
  return beta_r + beta_d;
}

/* He3-->He2 recombination cooling coefficient, beta_He2 */
inline __device__ rt_float RT_CUDA_recombcool_He2(rt_float const T,
                                                  int const recomb_approx) {
  rt_float pow1, pow2, pow3;
  /* Recombination cooling coefficient, J m^3 s^-1 */
  if (recomb_approx == Approx_A) {
    /* Seaton 1959 MNRAS 119 81 (multiplied by kT) */
    /* return 1.140e-39*SQRT(T)*(6.607 - LOG(T)/2. + 7.459e-3 * CBRT(T)); */
    /* HG97 is better for T>10^4. Scaled by 8*beta_B_HII(T/4) */
    pow1 = POW(T, -0.965);
    pow2 = POW(T, -0.502);
    pow3 = POW(1 + 1573. * pow2, -2.697);
    return 3.466e-30 * pow1 * pow3;
  } else { /* Case B */
    /* Hui & Gnedin 1997 MNRAS 292, 27, followed by the scaling:
       8*beta_B_HII(T/4) */
    pow1 = POW(T, -0.970);
    pow2 = POW(T, -0.376);
    pow3 = POW(1 + 145.1 * pow2, -3.720);
    return 7.183e-31 * pow1 * pow3;
  }
}
