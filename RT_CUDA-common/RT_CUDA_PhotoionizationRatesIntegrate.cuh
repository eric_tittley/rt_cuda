/* PhotoionizationRatesIntegrate: Evaluate the photoionisation and
 * photoionisation heating rate integral.  Specifically, divide the integral
 * into three parts and evaluate the integral using a method depending on
 * compile-time configuration.
 *
 * int PhotoionizationRatesIntegrate(rt_float N_H1, rt_float N_He1, rt_float
 * N_He2, rt_float N_H1_cum, rt_float N_He1_cum, rt_float N_He2_cum, rt_float z,
 * rt_float f[7])
 *
 * ARGUMENTS
 *  Input, not modified
 *   N_H1, N_He1, N_He2         Column densities in the cell. [m^2]
 *   N_H1_cum, N_He1_cum, N_He2_cum     Cumulative column densities between
 *   z          Redshift (used if the source is a function of time).
 *
 *  Output
 *   f[7]       Rates at frequency, nu. [I_H1, I_He1, I_He2, G_H1, G_He1, G_He2]
 *              I_* are the photoionisation Rates (* h_p). [s^-1]
 *              G_* are the heating Rates. [J s^-1]
 *              Gamma_HI is the photoionization rate per particle [m^3 s^-1
 * Hz^-1]
 *
 *
 * NOTE
 *  Splits the integral into three spans (nu_0 to nu_1), (nu_1 to nu_2), and
 *  (nu_2 to infinity).  Over these spans, the attenuated source spectrum is
 *  continuous.
 *
 * EXTERNAL MACROS AND VARIABLES
 *  N_Levels_1, N_Levels_2, N_Levels_3
 *
 * NOTES
 *
 * CALLS
 *  Gauss_Legendre()
 *  Gauss_Laguerre()
 *
 * AUTHOR: Eric Tittley
 */
#pragma once
#include "RT_CUDA.h"

#ifdef DEBUG
#  include <nppdefs.h>
#endif

/**********************************************************************
 * Integration via parts: nu_0 to nu_1, nu_1 to nu_2, nu_2 to nu_end. *
 * This assists convergence since it avoid the discontinuities at     *
 * these frequencies.                                                 *
 **********************************************************************/
inline __device__ RT_CUDA_PhotoRates RT_CUDA_PhotoionizationRatesIntegrate(
    rt_float const N_H1_local,
    rt_float const N_He1_local,
    rt_float const N_He2_local,
    rt_float const column_H1_local,
    rt_float const column_He1_local,
    rt_float const column_He2_local,
    RT_ConstantsT const* const Constants,
    RT_IntegralInvolatilesT const* const Involatiles) {
  /* Reset the rates to be summed */
  RT_CUDA_PhotoRates Rates{0};

  for (size_t iFrequency = 0; iFrequency < Involatiles->nFrequencies;
       ++iFrequency) {
    /* Evaluate the 7 functions at the frequency nu */
    auto Rates_i = RT_CUDA_PhotoionizationRatesFunctions(
        Involatiles->Frequencies[iFrequency],
        Involatiles->Luminosities[iFrequency],
        N_H1_local,
        N_He1_local,
        N_He2_local,
        column_H1_local,
        column_He1_local,
        column_He2_local,
        Constants,
        Involatiles->Alphas + 3 * iFrequency);

    /* Apply the weight */
    Rates.I_H1 += Rates_i.I_H1 * Involatiles->Weights[iFrequency];
    Rates.I_He1 += Rates_i.I_He1 * Involatiles->Weights[iFrequency];
    Rates.I_He2 += Rates_i.I_He2 * Involatiles->Weights[iFrequency];
    Rates.G_H1 += Rates_i.G_H1 * Involatiles->Weights[iFrequency];
    Rates.G_He1 += Rates_i.G_He1 * Involatiles->Weights[iFrequency];
    Rates.G_He2 += Rates_i.G_He2 * Involatiles->Weights[iFrequency];
    Rates.Gamma_HI += Rates_i.Gamma_HI * Involatiles->Weights[iFrequency];
#ifdef DEBUG
    rt_float f[7] = {Rates.I_H1,
                     Rates.I_He1,
                     Rates.I_He2,
                     Rates.G_H1,
                     Rates.G_He1,
                     Rates.G_He2,
                     Rates.Gamma_HI};
    for (size_t i = 0; i < 7; i++) {
      if ((!isfinite(f[i])) || (f[i] < -NPP_MINABS_32F)) {
        printf("%s: %i: ERROR: f[%lu]=%e; iFrequency=%lu\n",
               __FILE__,
               __LINE__,
               i,
               f[i],
               iFrequency);
      }
    }
#endif
  }

  return Rates;
}
