#include <nppdefs.h>

#include "RT_CUDA.h"
#include "cudasupport.h"

__global__ void RC_RTDCD_kernel(RT_Data const Data_dev,
                                int const LineNumber,
                                bool * const RT_RTDCD_ErrorFlag) {
  size_t const istart = blockIdx.x * blockDim.x + threadIdx.x;
  size_t const span = gridDim.x * blockDim.x;  // nBlocks * nThreadsPerBlock

#if 0
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
        if(cellindex==0) {
          printf("R=%p; dR=%p; Density=%p; Entropy=%p;\n",Data_dev.R, Data_dev.dR, Data_dev.Density, Data_dev.Entropy);
        }
        if(cellindex < 4) {
      printf("\nR[%lu]=%5.3e; dR[%lu]=%5.3e; Density[%lu]=%5.3e; Entropy[%lu]=%5.3e; T[%lu]=%5.3e\n",
             cellindex,Data_dev.R[cellindex],
             cellindex,Data_dev.dR[cellindex],
             cellindex,Data_dev.Density[cellindex],
             cellindex,Data_dev.Entropy[cellindex],
             cellindex,Data_dev.T[cellindex]
            );
    }
    if(cellindex > Data_dev.NumCells-4) {
      printf("\nR[%lu]=%5.3e; dR[%lu]=%5.3e; Density[%lu]=%5.3e; Entropy[%lu]=%5.3e; T[%lu]=%5.3e\n",
             cellindex,Data_dev.R[cellindex],
             cellindex,Data_dev.dR[cellindex],
             cellindex,Data_dev.Density[cellindex],
             cellindex,Data_dev.Entropy[cellindex],
             cellindex,Data_dev.T[cellindex]
            );
    }
  }
#endif

  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.R[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.R[cellindex])) {
     printf("ERROR: %i: R[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.R[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.dR[cellindex] <= NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.dR[cellindex])) {
      printf("ERROR: %i: dR[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.dR[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.Density[cellindex] <= NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.Density[cellindex])) {
      printf("ERROR: %i: Density[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.Density[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.Entropy[cellindex] <= NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.Entropy[cellindex])) {
      printf("ERROR: %i: Entropy[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.Entropy[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.T[cellindex] <= NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.T[cellindex])) {
      printf("ERROR: %i: T[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.T[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.n_H[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.n_H[cellindex])) {
      printf("ERROR: %i: n_H[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.n_H[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.f_H1[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.f_H1[cellindex])) {
      printf("ERROR: %i: f_H1[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.f_H1[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.f_H2[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.f_H2[cellindex])) {
      printf("ERROR: %i: f_H2[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.f_H2[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.n_He[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.n_He[cellindex])) {
      printf("ERROR: %i: n_He[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.n_He[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.f_He1[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.f_He1[cellindex])) {
      printf("ERROR: %i: f_He1[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.f_He1[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.f_He2[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.f_He2[cellindex])) {
      printf("ERROR: %i: f_He2[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.f_He2[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.f_He3[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.f_He3[cellindex])) {
      printf("ERROR: %i: f_He3[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.f_He3[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.column_H1[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.column_H1[cellindex])) {
      printf("ERROR: %i: column_H1[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.column_H1[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.column_He1[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.column_He1[cellindex])) {
      printf("ERROR: %i: column_He1[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.column_He1[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
  for (size_t cellindex = istart; cellindex < Data_dev.NumCells;
       cellindex += span) {
    if ((Data_dev.column_He2[cellindex] <= -NPP_MINABS_32F) ||
        !(bool)isfinite(Data_dev.column_He2[cellindex])) {
      printf("ERROR: %i: column_He2[%lu] is out of range: %5.3e;\n",
             LineNumber,
             cellindex,
             Data_dev.column_He2[cellindex]);
      *RT_RTDCD_ErrorFlag=true;
      return;
    }
  }
}

__device__ bool RT_RTDCD_ErrorFlag;

void RT_CUDA_RT_Data_CheckData(RT_Data const Data_dev,
                               const char * FileName,
                               int const LineNumber) {
  int blockSize;  // The launch configurator returned block size
  int gridSize;   // The actual grid size needed, based on input size
  setGridAndBlockSize(
      Data_dev.NumCells, (void*)RC_RTDCD_kernel, &gridSize, &blockSize);

  /* Allocate and set the Error Flag */
  bool *RT_RTDCD_ErrorFlag;
  ERT_CUDA_CHECK(cudaMalloc(&RT_RTDCD_ErrorFlag,sizeof(bool)));
  bool ErrorFlag=false;
  ERT_CUDA_CHECK(cudaMemcpy(RT_RTDCD_ErrorFlag,&ErrorFlag,sizeof(bool),cudaMemcpyHostToDevice));

  RC_RTDCD_kernel<<<gridSize, blockSize>>>(Data_dev, LineNumber, RT_RTDCD_ErrorFlag);

  /* Return the Error Flag */
  ERT_CUDA_CHECK(cudaMemcpy(&ErrorFlag,RT_RTDCD_ErrorFlag,sizeof(bool),cudaMemcpyDeviceToHost));
  ERT_CUDA_CHECK(cudaFree(RT_RTDCD_ErrorFlag));

  if(ErrorFlag) {
    printf("RT_Data ERROR in file %s line %i\n",FileName,LineNumber);
  }
}
