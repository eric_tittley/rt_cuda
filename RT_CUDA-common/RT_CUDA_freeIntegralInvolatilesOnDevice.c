
#include <stdlib.h>

#include "RT_CUDA.h"
#include "RT_IntegralInvolatilesT.h"
#include "cudasupport.h"

int RT_CUDA_freeIntegralInvolatilesOnDevice(
    RT_IntegralInvolatilesT Involatiles_d) {
  cudaFree(Involatiles_d.Frequencies);
  checkCUDAErrors(
      "Unable to free device memory for Frequencies", __FILE__, __LINE__);
  cudaFree(Involatiles_d.Weights);
  checkCUDAErrors(
      "Unable to free device memory for Weights", __FILE__, __LINE__);
  cudaFree(Involatiles_d.Luminosities);
  checkCUDAErrors(
      "Unable to free device memory for Luminosities", __FILE__, __LINE__);
  cudaFree(Involatiles_d.Alphas);
  checkCUDAErrors(
      "Unable to free device memory for Alphas", __FILE__, __LINE__);

  return EXIT_SUCCESS;
}
