/* PREPROCESSOR MACROS
 *  RT_DATA_SHORT (in RT_Data.h)
 *
 * AUTHOR: Eric Tittley
 */

#include "RT_CUDA.h"

__device__ void RT_CUDA_LastIterationUpdate(
    size_t const cellindex,
    RT_ConstantsT const* const Constants,
    RT_RatesT const* const Rates,
    RT_Data const* const Data_dev) {
  Data_dev->T[cellindex] = RT_CUDA_TemperatureFromEntropy(
      Data_dev->Entropy[cellindex], cellindex, Data_dev, Constants);
}
