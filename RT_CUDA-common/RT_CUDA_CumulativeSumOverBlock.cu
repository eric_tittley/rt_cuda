#include "RT_CUDA.h"

__device__ void RT_CUDA_CumulativeSumInPlaceOverBlock(rt_float* const A,
                                                      size_t const BlockWidth) {
  /* BlockWidth is the number of threads per block.
   * Assume BlockWidth is a power of two and A[BlockWidth].
   * Use padding if the total vector is shorter than BlockWidth. */
  unsigned int threadID = threadIdx.x;
  unsigned int step = 1;
  rt_float fetch;
  __syncthreads();
  fetch = (threadID > (step - 1)) ? (rt_float)((threadID) % 2) * A[threadID - 1]
                                  : 0.; /* 0 for even, a[i-1] for even */
  A[threadID] += fetch;
  __syncthreads();
  do {
    step *= 2;
    fetch = (threadID > (step - 1)) ? A[threadID + (threadID + 1) % 2 - step]
                                    : 0.; /* even: A[i-1], odd: A[i-2] */
    __syncthreads();                      /* If another warp is lagging */
    A[threadID] += fetch;
    __syncthreads(); /* If this warp is lagging. Same as if we put in front of
                      * the fetch, but improves exit synchonization */
  } while (step <= BlockWidth / 2);
}
