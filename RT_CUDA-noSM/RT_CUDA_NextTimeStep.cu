/* Determine the next timestep, dt_RT */
#include "RT_CUDA.h"

__device__ int RT_CUDA_NextTimeStep(rt_float const dt,
                                    rt_float const* const t_step,
                                    rt_float* const dt_RT,
                                    bool* const LastIterationFlag,
                                    rt_float const MIN_DT,
                                    rt_float const* const AdapScales,
                                    size_t const losid,
                                    size_t const numcells,
                                    rt_float const Timestep_Factor) {
  if (threadIdx.x == 0) {
#ifdef ADAPTIVE_TIMESCALE
    // TODO: Optimize
    size_t const adapindex = losid * numcells;
    dt_RT[0] = AdapScales[adapindex];
    size_t iz;
    for (iz = 0; iz < numcells; iz++) {
      if (dt_RT[0] > AdapScales[adapindex + iz])
        dt_RT[0] = AdapScales[adapindex + iz];
    }
    (dt_RT[0]) = Timestep_Factor * (dt_RT[0]);
#else
    dt_RT[0] = MIN_DT;
#endif

    if (t_step[0] + dt_RT[0] >= dt) {
      dt_RT[0] = dt - t_step[0];
      LastIterationFlag[0] = TRUE;
    }
  }

  return EXIT_SUCCESS;
}
