/* RT_AdaptiveTimescale: Update the various timescales.
 *
 * ierr = RT_CUDA_AdaptiveTimescale(size_t const losid,
 *                                  size_t const iz,
 *                                  size_t const cellindex,
 *                                  rt_float const dt,
 *                                  RT_ConstantsT const Constants,
 *                                  const RT_RatesT const *Rates,
 *                                  const RT_Data const *Data_dev,
 *                                  rt_float const *AdapScales);
 *
 * ARGUMENTS INPUT, NOT MODIFIED
 *  losid     The line of sight number
 *  iz        The cell number in the line of sight
 *  cellindex The index into the data arrays for this cell
 *  dt        The top-level time step
 *  Rates     The ionization, heating, and cooling rates
 *  Data_dev  The LOS data
 *  Constants Physical constants
 *
 * ARGUMENTS INPUT AND MODIFIED
 *  AdapScales[]        The minimum timescales array. Size: Nlos*NcellsPerLOS.
 *                      Only the timescale for this cell is updated.
 *                      Only the timescales in a single LOS are used to set the
 *                      timestep in this LOS.
 *
 * AUTHOR: Eric Tittley
 */

#include <stdlib.h>
/* #include <math.h> */

#include "RT_CUDA.h"

__device__ void RT_CUDA_AdaptiveTimescale(size_t const losid,
                                          size_t const iz,
                                          size_t const cellindex,
                                          rt_float const dt,
                                          RT_ConstantsT const* const Constants,
                                          RT_RatesT const* const Rates,
                                          RT_Data const* const Data_dev,
                                          rt_float* const AdapScales) {
  rt_float const InfiniteTime = 1e20;

  RT_TimeScale tau;

  size_t const adapindex = losid * (size_t)Data_dev->NumCells + iz;
  AdapScales[adapindex] = InfiniteTime;

  /* Find the neutral & ionised number densities. */
  rt_float const n_H1 = Data_dev->n_H[cellindex] * Data_dev->f_H1[cellindex];
  rt_float const n_H2 = Data_dev->n_H[cellindex] * Data_dev->f_H2[cellindex];
  rt_float const n_He1 = Data_dev->n_He[cellindex] * Data_dev->f_He1[cellindex];
  rt_float const n_He2 = Data_dev->n_He[cellindex] * Data_dev->f_He2[cellindex];
  rt_float const n_He3 = Data_dev->n_He[cellindex] * Data_dev->f_He3[cellindex];

  /* Electron density */
  rt_float const n_e = n_H2 + n_He2 + (2.0 * n_He3);

  /* Entropy timescale */
  rt_float ResidualHeatingScaleFactorGammaInv =
      POW(Constants->Ma, 1. / Constants->gamma_ad);
  rt_float const EntropyRate = FABS(
      (Constants->gamma_ad - 1.) /
      POW(Data_dev->Density[cellindex] * ResidualHeatingScaleFactorGammaInv,
          Constants->gamma_ad) *
      (Rates->G - Rates->L));
  if (EntropyRate > 0.0) {
    tau.Entropy = Data_dev->Entropy[cellindex] / EntropyRate;
  } else {
    tau.Entropy = InfiniteTime;
  }

  /*** TODO: MERGE IONIZATION ARE RECOMBINATION RATES INTO A dN/dt ***/

  /* Ionization timescales */
  if (Rates->I_H1 > 0.0) {
    tau.I_H1 = n_H1 / Rates->I_H1;
  } else {
    tau.I_H1 = InfiniteTime;
  }
  if ((Rates->I_He1 > 0.0) && (Constants->Y > 0.0)) {
    tau.I_He1 = n_He1 / Rates->I_He1;
  } else {
    tau.I_He1 = InfiniteTime;
  }
  if ((Rates->I_He2 > 0.0) && (Constants->Y > 0.0)) {
    tau.I_He2 = n_He2 / Rates->I_He2;
  } else {
    tau.I_He2 = InfiniteTime;
  }

  /* Recombination timescales */
  if (n_e > 0.0) {
    if (Rates->alpha_H1 > 0.0) {
      tau.R_H1 = 1.0 / (Rates->alpha_H1 * n_e);
    } else {
      tau.R_H1 = InfiniteTime;
    }
    if ((Rates->alpha_He1 > 0.0) && (Constants->Y > 0.0)) {
      tau.R_He1 = 1.0 / (Rates->alpha_He1 * n_e);
    } else {
      tau.R_He1 = InfiniteTime;
    }
    if ((Rates->alpha_He2 > 0.0) && (Constants->Y > 0.0)) {
      tau.R_He2 = 1.0 / (Rates->alpha_He2 * n_e);
    } else {
      tau.R_He2 = InfiniteTime;
    }
  } else {
    tau.R_H1 = InfiniteTime;
    tau.R_He1 = InfiniteTime;
    tau.R_He2 = InfiniteTime;
  }

  if (AdapScales[adapindex] > tau.T) AdapScales[adapindex] = tau.T;
  if (AdapScales[adapindex] > tau.I_H1) AdapScales[adapindex] = tau.I_H1;
  if (AdapScales[adapindex] > tau.I_He1) AdapScales[adapindex] = tau.I_He1;
  if (AdapScales[adapindex] > tau.I_He2) AdapScales[adapindex] = tau.I_He2;
  if (AdapScales[adapindex] > tau.R_H1) AdapScales[adapindex] = tau.R_H1;
  if (AdapScales[adapindex] > tau.R_He1) AdapScales[adapindex] = tau.R_He1;
  if (AdapScales[adapindex] > tau.R_He2) AdapScales[adapindex] = tau.R_He2;
}
