#ifdef HAVE_CONFIG
#  include "config.h"
#endif

#include "RT_CUDA.h"

/* Function to process each cell on a single thread and to share
 * the cells out between the threads.
 * This calls the further subroutines RT_CUDA_ProcessCell
 * that were part of the existing cuda implementation
 */
__device__ void RT_CUDA_ProcessCells(bool const LastIterationFlag,
                                     rt_float const ExpansionFactor,
                                     rt_float const tCurrent,
                                     rt_float const dt,
                                     rt_float const dt_RT,
                                     RT_SourceT const* const Source,
                                     RT_ConstantsT const* const Constants,
                                     RT_Data* const Data,
                                     size_t const losid,
                                     size_t const losoffset,
                                     rt_float* const AdapScales) {
  size_t iz;
  for (iz = threadIdx.x; iz < Data->NumCells; iz += blockDim.x) {
    /* Create single index for variable access */
    size_t const cellindex = losoffset + iz;

    RT_CUDA_ProcessCell(losid,
                        iz,
                        cellindex,
                        LastIterationFlag,
                        ExpansionFactor,
                        tCurrent,
                        dt,
                        dt_RT,
                        Source,
                        Constants,
                        Data,
                        AdapScales);
  }
}
